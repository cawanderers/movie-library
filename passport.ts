import {IUser, User} from './models/User';

const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const JWTStrategy = require('passport-jwt').Strategy;

const cookieExtractor = (req: any) => {
    return req?.cookies?.['access_token'];
};

passport.use(new JWTStrategy({
  jwtFromRequest: cookieExtractor,
  secretOrKey: 'SteveBai'
}, (payload: any, done: (err: any, user?: IUser) => void) => {
  User.findById({_id: payload.sub}, (err: any, user: IUser) => {
    if (err) {
      return done(err);
    }
    return done(null, user || false);
  });
}));

passport.use(new LocalStrategy((username: string, password: string, done: (err: any, user?: IUser | boolean) => void) => {
    User.findOne({username}, (err: any, user: IUser) => {
      if (err) {
        return done(err);
      }
      if (!user) {
        return done(undefined, false);
      }
      user.comparePassword(password, (err: any, isMatch: boolean) => {
        if (err) {
          return done(err);
        }
        if (isMatch) {
          return done(undefined, user);
        }
        return done(undefined, false);
      });
    })
  }
));
