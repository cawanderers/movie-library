import axios from "axios";
import { returnErrors } from "./errorActions";

export interface MovieRating {
  rate: number;
  movieId: number;
}

export const getMovieRatings = (id: number) => (
  dispatch: Function
) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const body = JSON.stringify({movieId: id});

  axios
    .post('/api/movie/rating/getMovieRatings', body, config)
    .then(({data}) =>
      dispatch({
        type: 'GET_MOVIE_RATINGS',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'GET_MOVIE_RATINGS_FAIL')
      );
    })
};

export const createMovieRating = ({rate, movieId}: MovieRating) => (
  dispatch: Function
) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const body = JSON.stringify({rate, movieId});

  axios
    .post('/api/movie/rating/createMovieRating', body, config)
    .then(({data}) =>
      dispatch({
        type: 'CREATE_MOVIE_RATING',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'CREATE_MOVIE_RATING_FAIL')
      );
    })
};

export const getMyMoviesRatings = () => (
  dispatch: Function
) => {
  axios
    .get('/api/movie/rating/getMyMoviesRatings')
    .then(({data}) =>
      dispatch({
        type: 'GET_MY_MOVIES_RATINGS',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'GET_MY_MOVIES_RATINGS_FAIL')
      );
    })
};

export const getMyMovieRating = (movieId: number) => (
  dispatch: Function
) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const body = JSON.stringify({movieId});

  axios
    .post('/api/movie/rating/getMyMovieRating', body, config)
    .then(({data}) =>
      dispatch({
        type: 'GET_MY_MOVIE_RATING',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'GET_MY_MOVIE_RATING_FAIL')
      );
    })
};

export const updateMyMovieRating = (id: string, {rate, movieId}: MovieRating) => (
  dispatch: Function
) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const body = JSON.stringify({rate, movieId});

  axios
    .patch(`/api/movie/rating/updateMyMovieRating/${id}`, body, config)
    .then(({data}) =>
      dispatch({
        type: 'UPDATE_MY_MOVIE_RATING',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'UPDATE_MY_MOVIE_RATING_FAIL')
      );
    })
};
