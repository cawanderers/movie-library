import axios from "axios";
import { returnErrors } from "./errorActions";
import { CollectedMovie } from "./collectedMoviesActions";

export const getMyFavoriteMovie = () => (
  dispatch: Function
) => {
  axios
    .get('/api/movie/favoriteMovie/getMyFavoriteMovie')
    .then(({data}) =>
      dispatch({
        type: 'GET_MY_FAVORITE_MOVIE',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'GET_MY_FAVORITE_MOVIE_FAIL')
      );
    })
};

export const markAsMyFavoriteMovie = ({ movieId, title, poster, rating, overview, releaseDate }: CollectedMovie) => (
  dispatch: Function
) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const body = JSON.stringify({ movieId, title, poster, rating, overview, releaseDate });

  axios
    .post('/api/movie/favoriteMovie/markAsMyFavoriteMovie', body, config)
    .then(({data}) =>
      dispatch({
        type: 'MARK_AS_MY_FAVORITE_MOVIE',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'MARK_AS_MY_FAVORITE_MOVIE_FAIL')
      );
    })
};

export const updateMyFavoriteMovie = (id: string, { movieId, title, poster, rating, overview, releaseDate }: CollectedMovie) => (
  dispatch: Function
) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const body = JSON.stringify({ movieId, title, poster, rating, overview, releaseDate });

  axios
    .patch(`/api/movie/favoriteMovie/updateMyFavoriteMovie/${id}`, body, config)
    .then(({data}) =>
      dispatch({
        type: 'UPDATE_MY_FAVORITE_MOVIE',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'UPDATE_MY_FAVORITE_MOVIE_FAIL')
      );
    })
};

export const unmarkAsMyFavoriteMovie = (id: string) => (
  dispatch: Function
) => {
  axios.delete(`/api/movie/favoriteMovie/unmarkAsMyFavoriteMovie/${id}`)
    .then(({data}) =>
      dispatch({
        type: 'UNMARK_AS_MY_FAVORITE_MOVIE',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'UNMARK_AS_MY_FAVORITE_MOVIE_FAIL')
      );
    })
};
