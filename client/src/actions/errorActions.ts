export interface ErrPayload {
  msg: string | any;
  status?: number;
  id: string
}

export const returnErrors = (msg: string | any, status: number, id: string) => {
  return {
    type: 'GET_ERRORS',
    payload: {msg, status, id}
  };
};

export const clearErrors = () => {
  return {
    type: 'CLEAR_ERRORS'
  };
};
