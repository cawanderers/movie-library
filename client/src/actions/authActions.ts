import axios from "axios";
import { returnErrors } from "./errorActions";

export interface Register {
  email: string;
  password: string;
  role: string;
  username: string;
}

export interface Login {
  username: string;
  password: string;
}

export const register = ({email, password, role, username}: Register) => (
  dispatch: Function
) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const body = JSON.stringify({email, password, role, username});

  axios
    .post('/api/user/register', body, config)
    .then(({data}) =>
      dispatch({
        type: 'REGISTER_SUCCESS',
        payload: data
      })
    )
    .catch(({response}) => {
      dispatch(
        returnErrors(response.data, response.status, 'REGISTER_FAIL')
      );
      dispatch({
        type: 'REGISTER_FAIL'
      });
    });
};

export const login = ({username, password}: Login) => (
  dispatch: Function
) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const body = JSON.stringify({username, password});

  axios
    .post('/api/user/login', body, config)
    .then(({data}) =>
      dispatch({
        type: 'LOGIN_SUCCESS',
        payload: data
      })
    )
    .catch(({response}) => {
      dispatch(
        returnErrors(response.data, response.status, 'LOGIN_FAIL')
      );
      dispatch({
        type: 'LOGIN_FAIL'
      });
    });
};

export const isAuthenticated = () => (
  dispatch: Function
) => {
  axios
    .get('/api/user/authenticated')
    .then(({data}) =>
      dispatch({
        type: 'AUTHENTICATED_SUCCESS',
        payload: data
      })
    )
    .catch(({response}) => {
      dispatch(
        returnErrors(response.data, response.status, 'AUTHENTICATED_FAIL')
      );
      dispatch({
        type: 'AUTHENTICATED_FAIL'
      });
    });
};


export const logout = () => (
  dispatch: Function
) => {
  axios
    .get('/api/user/logout')
    .then(({data}) =>
      dispatch({
        type: 'LOG_OUT',
        payload: data
      })
    );
};
