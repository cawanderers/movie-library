import axios from "axios";
import { IUser } from "../../../models/User";

const returnErrors = (msg: string | any, status: number, id: any) => {
  return {
    type: 'GET_ERRORS',
    payload: {msg, status, id}
  };
};

export const register = ({email, password, role, username}: IUser) => (dispatch: Function) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const body = JSON.stringify({email, password, role, username});

  axios
    .post('/api/user/register', body, config)
    .then(({ data }) =>
      dispatch({
        type: 'REGISTER_SUCCESS',
        payload: data
      })
    )
    .catch(response => {
      dispatch(
        returnErrors(response.response.data, response.response.status, 'REGISTER_FAIL')
      );
      dispatch({
        type: 'REGISTER_FAIL'
      });
    });
};
