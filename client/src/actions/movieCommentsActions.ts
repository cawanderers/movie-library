import axios from "axios";
import { returnErrors } from "./errorActions";

export interface MovieComment {
  content: string;
  movieId: number;
}

export const getMovieComments = (id: number) => (
  dispatch: Function
) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const body = JSON.stringify({movieId: id});

  axios
    .post('/api/movie/comment/getMovieComments', body, config)
    .then(({data}) =>
      dispatch({
        type: 'GET_MOVIE_COMMENTS',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'GET_MOVIE_COMMENTS_FAIL')
      );
    })
};

export const createMovieComment = ({content, movieId}: MovieComment) => (
  dispatch: Function
) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const body = JSON.stringify({content, movieId});

  axios
    .post('/api/movie/comment/createMovieComment', body, config)
    .then(({data}) =>
      dispatch({
        type: 'CREATE_MOVIE_COMMENT',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'CREATE_MOVIE_COMMENTS_FAIL')
      );
    })
};
