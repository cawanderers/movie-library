import axios from "axios";
import { returnErrors } from "./errorActions";

export interface UserProfile {
  firstName: string;
  lastName: string;
  gender: string;
  birthday: string;
  about: string;
  favouriteMovieGenre: string;
}

export const getMyProfile = () => (
  dispatch: Function
) => {
  axios
    .get('/api/user/profile/getMyProfile')
    .then(({data}) =>
      dispatch({
        type: 'GET_MY_PROFILE',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'GET_MY_PROFILE_FAIL')
      );
    })
};

export const createMyProfile = ({ firstName, lastName, gender, birthday, about, favouriteMovieGenre }: UserProfile) => (
  dispatch: Function
) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
};

  const body = JSON.stringify({ firstName, lastName, gender, birthday, about, favouriteMovieGenre });

  axios
    .post('/api/user/profile/createMyProfile', body, config)
    .then(({data}) =>
      dispatch({
        type: 'CREATE_MY_PROFILE',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'CREATE_MY_PROFILE_FAIL')
      );
    })
};

export const updateMyProfile = (id: string, { firstName, lastName, gender, birthday, about, favouriteMovieGenre }: UserProfile) => (
  dispatch: Function
) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
};

  const body = JSON.stringify({ firstName, lastName, gender, birthday, about, favouriteMovieGenre });

  axios
    .patch(`/api/user/profile/updateMyProfile/${id}`, body, config)
    .then(({data}) =>
      dispatch({
        type: 'UPDATE_MY_PROFILE',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'UPDATE_MY_PROFILE_FAIL')
      );
    })
};
