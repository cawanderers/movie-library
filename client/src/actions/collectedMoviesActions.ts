import axios from "axios";
import { returnErrors } from "./errorActions";

export interface CollectedMovie {
  movieId: number;
  title: string;
  poster: string;
  rating: number;
  overview: string;
  releaseDate: string;
}

export const getMyWatchlist = () => (
  dispatch: Function
) => {
  axios
    .get('/api/movie/collectedMovie/getMyWatchlist')
    .then(({data}) =>
      dispatch({
        type: 'GET_MY_WATCHLIST',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'GET_MY_WATCHLIST_FAIL')
      );
    })
};

export const addToWatchlist = ({ movieId, title, poster, rating, overview, releaseDate }: CollectedMovie) => (
  dispatch: Function
) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const body = JSON.stringify({ movieId, title, poster, rating, overview, releaseDate });

  axios
    .post('/api/movie/collectedMovie/addToWatchlist', body, config)
    .then(({data}) =>
      dispatch({
        type: 'ADD_TO_WATCHLIST',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'ADD_TO_WATCHLIST_FAIL')
      );
    })
};

export const getMovieFromWatchlist = (movieId: number) => (
  dispatch: Function
) => {
  const config = {
    headers: {
      'Content-Type': 'application/json'
    }
  };

  const body = JSON.stringify({movieId});

  axios
    .post('/api/movie/collectedMovie/getMovieFromWatchlist', body, config)
    .then(({data}) =>
      dispatch({
        type: 'GET_MOVIE_FROM_WATCHLIST_BY_MOVIE_ID',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'GET_MOVIE_FROM_WATCHLIST_BY_MOVIE_ID_FAIL')
      );
    })
};

export const removeFromWatchlist = (id: string) => (
  dispatch: Function
) => {
  axios
    .delete(`/api/movie/collectedMovie/removeFromWatchlist/${id}`)
    .then(({data}) =>
      dispatch({
        type: 'REMOVE_MOVIE_FROM_WATCHLIST',
        payload: data
      })
    )
    .catch(() => {
      dispatch(
        returnErrors('Something is wrong', 400, 'REMOVE_MOVIE_FROM_WATCHLIST_FAIL')
      );
    })
};
