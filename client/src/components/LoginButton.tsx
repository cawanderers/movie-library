import React, { useEffect } from "react";
import { StyledLink } from "./MyMenuItem";
import { Button, Icon } from "semantic-ui-react";
import { useDispatch, useSelector } from "react-redux";
import { IAuth } from "./RegisterForm";
import { isAuthenticated } from "../actions/authActions";
import LoginDropdown from "./LoginDropdown";
import { sendGAEvent } from "../utils";

const LoginButton = () => {
  const dispatch = useDispatch();
  const auth = useSelector(((state: IAuth) => state.auth));
  const username = auth?.user?.username;

  useEffect(() => {
    dispatch(isAuthenticated());
  }, [dispatch])

  return username ? <LoginDropdown username={username} /> : (
    <StyledLink to={'/login'}>
      <Button size={'small'} basic color='blue' onClick={() => sendGAEvent('Click the login Button')}>
        <Icon name='user' />
        Sign in
      </Button>
    </StyledLink>
  )
};

export default LoginButton;
