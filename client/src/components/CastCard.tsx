import React from "react";

const CastCard = ({cast}: { cast: any }) => {
  return (
    <>
      <div className="ui card">
        <div className="image">
          <img style={{borderRadius: 0}} src={`https://image.tmdb.org/t/p/w185/${cast.profile_path}`} alt={cast.name} />
        </div>
      </div>
      <h5 style={{textAlign: 'center'}}>{cast.name}</h5>
      <div style={{textAlign: 'center'}} className="meta">
        {cast.character}
      </div>
    </>
  );
};

export default CastCard;
