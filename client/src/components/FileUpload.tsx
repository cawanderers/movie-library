import React from "react";
import Thumb from "./Thumb";

const FileUpload = ({label, formik}: { label: string, formik: any }) => {
  return (
    <div className="field">
      <label htmlFor='file'>{label}</label>
      <Thumb file={formik.values.file} />
      <input id='file' name='file' type='file'
             onChange={(event) => event?.currentTarget?.files?.[0] && formik.setFieldValue("file", event.currentTarget.files[0])} />
    </div>
  );
};

export default FileUpload;
