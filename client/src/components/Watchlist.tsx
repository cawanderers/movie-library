import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMyWatchlist } from "../actions/collectedMoviesActions";
import { Header, Item } from "semantic-ui-react";
import WatchlistItem from "./WatchlistItem";

export interface WatchlistMovie {
  _id: string;
  movieId: number;
  title: string;
  poster: string;
  rating: number;
  overview: string;
  releaseDate: string;
}

const Watchlist = () => {
  const collectedMovies = useSelector(((state: any) => state.collectedMovies));
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMyWatchlist());
  }, [dispatch]);

  const moviesList = () => {
    return collectedMovies?.length ? (
      collectedMovies?.map((collectedMovie: WatchlistMovie) => (
        <WatchlistItem key={collectedMovie.movieId} _id={collectedMovie._id} movieId={collectedMovie.movieId} title={collectedMovie.title}
                       poster={collectedMovie.poster}
                       rating={collectedMovie.rating} overview={collectedMovie.overview}
                       releaseDate={collectedMovie.releaseDate} />
      ))) : <>You haven't added any movies to your watchlist</>
  };

  return (
    <div className="column twelve wide">
      <Header as='h1'>My Watchlist</Header>
      <Item.Group divided>
        {moviesList()}
      </Item.Group>
    </div>
  );
};

export default Watchlist;
