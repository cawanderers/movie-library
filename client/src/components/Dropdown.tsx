import React, { useState, useEffect, useRef } from 'react';
import { Link, useLocation } from 'react-router-dom';
import styled from 'styled-components';
import { sendGAEvent, genresPaths, isCurrentPath } from "../utils";
import { url } from "../contants";

const StyledLink = styled(Link)`
  color: black;
  text-decoration: none;
  display: block;
  outline: none;
  padding: 8px 14px 5px;
`;

const Dropdown = ({label, options, selected, onSelectedChange}: {
  label: string, options: any, selected: any, onSelectedChange: any
}) => {
  const [open, setOpen] = useState(false);
  const ref = useRef<HTMLHeadingElement>(null);

  const location = useLocation();

  useEffect(() => {
    const onBodyClick = (event: any) => !ref.current?.contains(event.target) && setOpen(false);

    document.body.addEventListener('click', onBodyClick);

    return () => {
      document.body.removeEventListener('click', onBodyClick);
    };
  }, []);

  const renderedOptions = options.map((option: any) => {
    if (option.value === selected.value) {
      return null;
    }

    return (
      <StyledLink
        to={isCurrentPath(location.pathname, genresPaths) ? `${url}/discover/Genres/${option.label}` : `${url}/filter/${option.label}`}
        onClick={() =>
          {
            onSelectedChange(option);
            sendGAEvent(`Click option of ${option.label}`)
          }
        }
        key={option.value}
      >
        <div className="item">
          {option.label}
        </div>
      </StyledLink>
    )
  })

  return (
    <div ref={ref} className="ui form">
      <div className="six fields">
        <div className="field">
          <label className="label">{label}</label>
          <div onClick={() => setOpen(!open)} className={`ui selection dropdown ${open ? 'visible active' : ''}`}>
            <i className="dropdown icon" />
            <div className="text">{selected.label}</div>
            <div className={`menu ${open ? 'visible transition' : ''}`}>
              {renderedOptions}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dropdown;
