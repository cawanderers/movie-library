import React from 'react';
import styled from 'styled-components';
import PageButton from "./PageButton";

interface StyledDivProps {
  type: string;
}

const Wrapper = styled.div<StyledDivProps>`
  margin-top: 20px;
  display: flex;
  align-items: center;
  justify-content: ${props => {
    if (props.type === 'one') {
      return 'flex-start';
    } else if (props.type === 'both') {
      return 'space-between';
    } else {
      return 'flex-end';
    }
  }};
`;

const Pagination = ({page, totalPages}: { page: number, totalPages: number }) => {

  if (totalPages === 1) {
    return null;
  }

  if (page < totalPages && page === 1) {
    return (
      <Wrapper type='right'>
        <PageButton page={page + 1} icon={'right arrow'} position={'right'} />
      </Wrapper>
    );
  } else if (page < totalPages) {
    return (
      <Wrapper type="both">
        <PageButton page={page - 1} icon={'left arrow'} position={'left'} />
        <PageButton page={page + 1} icon={'right arrow'} position={'right'} />
      </Wrapper>
    );
  } else {
    return (
      <Wrapper type="one">
        <PageButton page={page - 1} icon={'left arrow'} position={'left'} />
      </Wrapper>
    );
  }
};

export default Pagination;
