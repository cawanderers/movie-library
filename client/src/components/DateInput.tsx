import React from 'react';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import { getLabel } from "../utils";

const DateInput = ({name, formik}: { name: string, formik: any }) => {
  const label = getLabel(name);

  return (
    <div className="field">
      <label>{label}</label>
      <DatePicker
        selected={formik.values.birthday}
        name={name}
        onChange={date => formik.setFieldValue('birthday', date)}
        placeholderText='MM/DD/YYYY'
        peekNextMonth
        showMonthDropdown
        showYearDropdown
        dropdownMode="select"
      />
    </div>
  );
};

export default DateInput;
