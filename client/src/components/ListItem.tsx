import React from "react";
import './ListItem.scss';

const ListItem = ({ fieldValue, matcher, content }: { fieldValue: string, matcher: RegExp, content: string }) => {
  return (
    <div
      className={`item validator ${fieldValue.match(matcher) ? "valid" : "invalid"}`}>
      {content}
    </div>
  );
};

export default ListItem;
