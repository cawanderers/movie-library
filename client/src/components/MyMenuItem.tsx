import React from "react";
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { MenuOption } from "./SideMenu";
import { Menu } from "semantic-ui-react";
import "./MyMenuItem.scss";

export const StyledLink = styled(Link)`
  color: white;
  text-decoration: none;
  display: block;
  margin-bottom: 20px;
  font-weight: 700;
  font-size: 15px;
  margin-top: 30px;
`;

const MyMenuItem = ({option, selected, onSelectedChange}: { option: MenuOption, selected: string, onSelectedChange: (value: string) => void }) => {
  return (
    <Menu.Item
      as="a"
      href={option.link}
      name={option.label}
      active={selected === option.label}
      onClick={() => onSelectedChange(option.label)}
    >
      <div className="MyMenuItemContainer">
        <i className={option.icon} />
      </div>
      {option.label}
    </Menu.Item>
  );
};

export default MyMenuItem;
