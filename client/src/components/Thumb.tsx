import React, { useState, useEffect } from "react";

const defaultAvatar = 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Faenza-avatar-default-symbolic.svg/1024px-Faenza-avatar-default-symbolic.svg.png';

const Thumb = ({file}: { file: any }) => {
  const [loading, setLoading] = useState(false);
  const [thumb, setThumb] = useState<any | null>(undefined);

  useEffect(() => {
    if (!file) {
      return;
    }
    setLoading(true);
  }, [file]);

  useEffect(() => {
    if (loading) {
      let reader = new FileReader();
      reader.onloadend = () => {
        setLoading(false);
        setThumb(reader.result);
      }
      reader.readAsDataURL(file);
    }
  }, [file, loading])

  if (loading) {
    return <p>loading...</p>;
  }
  return (
    <img src={thumb || defaultAvatar} alt='avatar' className='ui image' height={100}
         width={100} />
  );
};

export default Thumb;
