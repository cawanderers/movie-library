import React, { useEffect, useState } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import CarouselArrowNext from "./CarouselArrowNext";
import CarouselArrowPrev from "./CarouselArrowPrev";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import tmdb from "../api/tmdb";
import MyLoader from "./MyLoader";
import ImageCard from "./ImageCard";

const MoviePicsCarousel = ({selected, movieId}: { selected: string, movieId: number }) => {
  const [posters, setPosters] = useState<any[] | undefined>([]);
  const [backdrops, setBackdrops] = useState<any[] | undefined>([]);
  const [loading, setLoading] = useState(false);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    swipeToSlide: true,
    slidesToShow: selected === 'Backdrops' ? 2 : 4,
    slidesToScroll: 1,
    nextArrow: <CarouselArrowNext />,
    prevArrow: <CarouselArrowPrev />
  };

  useEffect(() => {
    const getImages = async () => {
      const {data: {backdrops, posters}} = await tmdb.getMovieImages(movieId);
      setBackdrops(backdrops);
      setPosters(posters);
    };
    getImages()
      .then(() => setLoading(false));

    return () => setLoading(true)
  }, [movieId]);

  const getVisibleImages = (images: any[] | undefined) => {
    return images?.slice(0, 7)?.map((el) => {
      return (
        <ImageCard key={el.file_path} image={el} selected={selected} />
      );
    });
  };

  const visibleBackdrops = getVisibleImages(backdrops);
  const visiblePosters = getVisibleImages(posters);

  return loading ? <MyLoader /> : (
    <Slider {...settings}>
      {selected === 'Backdrops' ? visibleBackdrops : visiblePosters}
    </Slider>
  );
};

export default MoviePicsCarousel;
