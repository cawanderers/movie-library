import React from "react";
import { useLocation } from 'react-router-dom';
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import styled from 'styled-components';
import { Genres, genresPaths, isCurrentPath } from "../utils";

const StyledMultiselect = styled(Select)`
  margin-top: 4px;
  margin-bottom: 1em;
  width: 210px;
`;

const animatedComponents = makeAnimated();

const GenresFilter = ({
                        movieGenres,
                        selected,
                        onSelectedChange,
                      }: { movieGenres: Genres[], selected: any, onSelectedChange: any }) => {
  const validGenres = movieGenres.filter(({value}: { value: string }) => value !== '-');
  const onChange = (selectedOptions: string[]) => onSelectedChange(selectedOptions);

  const location = useLocation();

  return isCurrentPath(location.pathname, genresPaths) ? (
    <>
      <label className="label" style={{fontWeight: 700}}>Genres</label>
      <StyledMultiselect
        closeMenuOnSelect={false}
        components={animatedComponents}
        isMulti
        value={selected}
        onChange={onChange}
        options={validGenres}
      />
    </>
  ) : null;
};

export default GenresFilter;
