import React from "react";
import './ImageCard.scss';
import { imageSrc } from "../utils";

const ImageCard = ({image, selected}: { image: any, selected: string }) => {

  return (
    <img
      className={`${selected === 'Backdrops' ? 'backdropsWidth' : 'posterWidth'}`}
      src={`${imageSrc}${image.file_path}`}
      alt='movieImages' />
  );
};

export default ImageCard;
