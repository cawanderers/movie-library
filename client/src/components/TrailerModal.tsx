import React, { useEffect, useState } from "react";
import { Button, Modal } from 'semantic-ui-react'
import tmdb from "../api/tmdb";

const TrailerModal = ({movieId}: { movieId: number }) => {
  const [videos, setVideos] = useState<any[] | undefined>([]);
  const [open, setOpen] = useState<any | undefined>(false)

  useEffect(() => {
    const getTrailer = async () => {
      const {data: {results}} = await tmdb.getMovieTrailer(movieId);
      setVideos(results);
    };
    getTrailer();
  }, [movieId]);

  const video = videos?.find(
    (video) => video.type === 'Trailer' && video.site === 'YouTube'
  );

  if (!video) {
    return <>Loading...</>
  }

  const videoSrc = `https://www.youtube.com/embed/${video.key}?autoplay=1`;
  return (
    <Modal
      closeOnEscape={false}
      closeOnDimmerClick={false}
      closeIcon
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      trigger={<Button icon='play' content='Play Trailer' primary />}
    >
      <div className="ui embed">
        <iframe src={videoSrc} allowFullScreen={true} title="video player" />
      </div>
    </Modal>
  );
};

export default TrailerModal;
