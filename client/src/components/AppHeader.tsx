import React from "react";
import { Grid } from "semantic-ui-react";
import MySearchBar from "./MySearchBar";
import LoginButton from "./LoginButton";
import { useLocation } from "react-router-dom";
import { isCurrentPath, pagePaths } from "../utils";

const AppHeader = () => {
  const location = useLocation();

  return isCurrentPath(location.pathname, pagePaths) ? null : (
    <Grid>
      <Grid.Row>
        <Grid.Column width={14}>
          <MySearchBar />
        </Grid.Column>
        <Grid.Column width={2}>
          <LoginButton />
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

export default AppHeader;
