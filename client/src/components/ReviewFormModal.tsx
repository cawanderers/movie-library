import React, { useEffect, useState } from "react";
import { Button, Header, Modal } from "semantic-ui-react";
import { useFormik } from 'formik';
import './ReviewFormModal.scss';
import { createMovieComment, MovieComment } from "../actions/movieCommentsActions";
import { useDispatch, useSelector } from "react-redux";
import { IAuth } from "./RegisterForm";
import { Link } from 'react-router-dom';
import MyTextarea from "./MyTextarea";
import { getMyMovieRating } from "../actions/movieRatingsActions";
import EditableMovieRating from "./EditableMovieRating";
import { sendGAEvent } from "../utils";

const validate = (values: MovieComment) => {
  const errors: any = {};
  if (!values.content) {
    errors.content = 'Required';
  }
  return errors;
};

const ReviewFormModal = ({movieId, movieTitle}: { movieId: number, movieTitle: string }) => {
  const [open, setOpen] = useState<boolean>(false);
  const [movieScore, setMovieScore] = useState(0);
  const dispatch = useDispatch();
  const auth = useSelector(((state: IAuth) => state.auth));
  const ratings = useSelector(((state: any) => state.ratings));
  const formik = useFormik({
    initialValues: {
      content: '',
      movieId: movieId,
    },
    validate,
    onSubmit: async (values, {resetForm}) => {
      dispatch(createMovieComment(values));
      setOpen(false);
      resetForm();
    },
  });

  useEffect(() => {
    dispatch(getMyMovieRating(movieId));
    if (ratings?.length) {
      setMovieScore(ratings[0].rate);
    }
  }, [dispatch, movieId, ratings]);

  return auth.isAuthenticated ? (
      <Modal
        closeOnEscape={false}
        closeOnDimmerClick={false}
        closeIcon
        onClose={() => setOpen(false)}
        onOpen={() => setOpen(true)}
        open={open}
        trigger={
          <Button className="reviewModalButton" size='small' basic icon='add' content='Review' onClick={() => sendGAEvent('Open review model')}/>
        }
      >
        <div className="ui form ReviewModalContainer">
          <Header as='h5'>Your Rating</Header>
          <EditableMovieRating movieScore={movieScore} movieId={movieId} movieTitle={movieTitle}/>
          <form onSubmit={formik.handleSubmit}>
            <MyTextarea name={'content'} formik={formik} />
            <button className="ui fluid primary button" type="submit">Submit</button>
          </form>
        </div>
      </Modal>
    ) :
    <Link className="reviewModalButton" to={'/login'}>
      <Button size='small' basic icon='add' content='Review' />
    </Link>
};

export default ReviewFormModal;
