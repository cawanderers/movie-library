import React, { useState } from "react";
import styled from "styled-components";
import MovieRating from "./MovieRating";
import { getMovieInfo, splitYear } from "../utils";

const ReadMoreWrapper = styled.span`
  color: rgb(110, 110, 110);
  cursor: pointer;
`;

interface Spoken_Languages {
  english_name: string,
  iso_639_1: string,
  name: string
}

interface Movie {
  runtime: number,
  tagline: string,
  title: string,
  vote_average: number,
  release_date: string,
  overview: string,
  spoken_languages: Spoken_Languages[]
}

const MovieDetails = styled.div`
  margin-top: 20px;
  margin-bottom: 30px;
`;

const RatingsWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-right: auto;
`;

const Info = styled.div`
  font-weight: 700;
  line-height: 1;
  font-size: 1rem;
`;

const MovieInfo = ({movie}: { movie: Movie }) => {
  const [isReadMore, setIsReadMore] = useState(true);
  const toggleReadMore = () => setIsReadMore(!isReadMore);

  const info = getMovieInfo(
    movie.spoken_languages,
    movie.runtime,
    splitYear(movie.release_date)
  );

  return (
    <MovieDetails>
      <h1 style={{marginBottom: 30}} className="ui header">
        {movie.title}
        <div className="sub header">{movie.tagline}</div>
      </h1>
      <div style={{marginBottom: 30}} className="ui right aligned grid">
        <RatingsWrapper>
          <MovieRating number={movie.vote_average / 2} />
          <span style={{marginLeft: 6, color: '#E09015'}}>{movie.vote_average}</span>
        </RatingsWrapper>
        <Info>
          {info}
        </Info>
      </div>
      <h2 className="ui header">Overview</h2>
      <p style={{fontSize: 16}}>
        {isReadMore ? movie?.overview?.slice(0, 130) : movie?.overview}
        <ReadMoreWrapper onClick={toggleReadMore}>
          {isReadMore ? " ...read more" : " show less"}
        </ReadMoreWrapper>
      </p>
    </MovieDetails>
  );
};

export default MovieInfo;
