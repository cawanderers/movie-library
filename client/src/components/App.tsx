import React, { useEffect, useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import MoviesListByFilter from "./MoviesListByFilter";
import { library } from "@fortawesome/fontawesome-svg-core";
import { far } from "@fortawesome/free-regular-svg-icons";
import { fas } from "@fortawesome/free-solid-svg-icons";
import MoviesListByGenres from "./MoviesListByGenres";
import MovieContainer from "./MovieContainer";
import SideMenu from "./SideMenu";
import { movieGenres } from "../utils";
import GenresFilter from "./GenresFilter";
import { url } from "../contants";
import LoginForm from "./LoginForm";
import { Container } from "semantic-ui-react";
import AppHeader from "./AppHeader";
import RegisterForm from "./RegisterForm";
import Watchlist from "./Watchlist";
import EditProfile from "./EditProfile";
import ReactGa from 'react-ga';
import MobileAppHeader from "./MobileAppHeader";

library.add(far, fas);

const App = () => {
  const [genres, setGenres] = useState([]);
  const [showSidebar, setShowSidebar] = useState(false);
  const [isMobile, setIsMobile] = useState(false);

  const changeToMobile = () => {
    setIsMobile(window.matchMedia('(max-width:768px)').matches);
  };

  useEffect(() => {
    changeToMobile();
    window.addEventListener('resize', changeToMobile);

    return () => window.removeEventListener('resize', changeToMobile);
  }, []);

  useEffect(() => {
    ReactGa.initialize('UA-211058407-1');

    ReactGa.pageview(window.location.pathname + window.location.search);
  }, []);

  return (
    <Router>
      <Container>
        {isMobile ? <MobileAppHeader visible={showSidebar} setVisible={setShowSidebar} /> :
          <AppHeader />}
        <div className="ui container grid">
          <div className="column four wide" style={{marginTop: 50}}>
            <SideMenu />
            <GenresFilter movieGenres={movieGenres} selected={genres} onSelectedChange={setGenres} />
          </div>
          <Switch>
            <Route
              path={url + '/'}
              exact
              render={() => (
                <Redirect
                  from={url + '/'}
                  to={url + '/filter/Popular'}
                />
              )}
            />
            <Route
              path={url + '/filter/:name'}
              exact
              component={MoviesListByFilter}
            />
            <Route
              path={url + '/discover/genres/:name'}
              exact
            >
              <MoviesListByGenres genres={genres} />
            </Route>
            <Route
              path={url + '/movie/:id'}
              exact
              component={MovieContainer}
            />
            <Route
              path={url + '/login'}
              exact
              component={LoginForm}
            />
            <Route
              path={url + '/register'}
              exact
              component={RegisterForm}
            />
            <Route
              path={url + '/:username/watchlist'}
              exact
              component={Watchlist}
            />
            <Route
              path={url + '/user/profile'}
              exact
              component={EditProfile}
            />
          </Switch>
        </div>
      </Container>
    </Router>
  );
};

export default App;
