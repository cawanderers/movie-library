import React, { useEffect, useState } from "react";
import tmdb from "../api/tmdb";
import Poster from "./Poster";
import MovieInfo from "./MovieInfo";
import TrailerModal from "./TrailerModal";
import DetailButtons from "./DetailButtons";
import CastCarousel from "./CastCarousel";
import MyTab from "./MyTab";
import MoviesByTab from "./MoviesByTab";
import MyLoader from "./MyLoader";
import { movieImagesTabOptions, movieTabOptions } from "../utils";
import MoviePicsCarousel from "./MoviePicsCarousel";
import MovieReviewsList from "./MovieReviewsList";

const MovieContainer = ({match}: { match: any }) => {
  const [movie, setMovie] = useState<any | undefined>({});
  const [activeItem, setActiveItem] = useState('Recommendations');
  const [activeTab, setActiveTab] = useState('Backdrops');
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    const getMovie = async () => {
      const {data} = await tmdb.getSingleMovie(match.params.id);
      setMovie(data);
    };
    getMovie()
      .then(() => setLoading(false));

    return () => setLoading(true);
  }, [match.params.id]);

  return loading ? <MyLoader /> : (
    <div className="column twelve wide">
      <div style={{marginBottom: 50}} className="ui grid">
        <div className="seven wide column">
          <Poster url={movie.poster_path} id={movie.id} />
        </div>
        <div className="nine wide column">
          <MovieInfo movie={movie} />
          <div className="ui right aligned grid">
            <DetailButtons movieId={movie.id} title={movie.title} poster={movie.poster_path} rating={movie.vote_average} overview={movie.overview} releaseDate={movie.release_date} />
            <TrailerModal movieId={movie.id} />
          </div>
        </div>
      </div>
      <CastCarousel movieId={movie.id} />
      <MyTab options={movieTabOptions} selected={activeItem} onSelectedChange={setActiveItem} />
      <MoviesByTab selected={activeItem} movieId={movie.id} />
      <h3 className="ui header">Movie Images</h3>
      <MyTab options={movieImagesTabOptions} selected={activeTab} onSelectedChange={setActiveTab} />
      <MoviePicsCarousel selected={activeTab} movieId={movie.id} />
      <MovieReviewsList movieId={match.params.id} movieTitle={movie.title} />
    </div>
  );
};

export default MovieContainer;
