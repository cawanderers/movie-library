import React from "react";
import { Feed } from "semantic-ui-react";
import { PostedBy } from "./MovieReviewsList";
import { getTimeFromNow } from "../utils";

const MovieReviewsItem = ({
                            content,
                            createdAt,
                            postedBy
                          }: { content: string, createdAt: string, postedBy: PostedBy }) => {
  return (
    <Feed.Event>
      <Feed.Content>
        <Feed.Summary>
          {postedBy.username} posted
          <Feed.Date>
            <>{getTimeFromNow(createdAt)}</>
          </Feed.Date>
        </Feed.Summary>
        <Feed.Extra text>{content}</Feed.Extra>
      </Feed.Content>
    </Feed.Event>
  );
};

export default MovieReviewsItem;
