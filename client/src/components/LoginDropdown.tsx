import React from "react";
import { Dropdown } from "semantic-ui-react";
import './LoginButton.scss';
import { useDispatch } from "react-redux";
import { logout } from "../actions/authActions";

interface DropdownLink {
  name: string;
  link: string;
}

const LoginDropdown = ({username}: { username: string }) => {
  const dispatch = useDispatch();

  const dropdownLinks: DropdownLink[] = [
    {
      name: 'Watchlist',
      link: `/${username}/watchlist`
    },
    {
      name: 'Edit Profile',
      link: '/user/profile'
    },
  ];

  const linkItems = dropdownLinks.map((dropdownLink ) => {
    return (
      <Dropdown.Item key={dropdownLink.name} as='a' href={`${dropdownLink.link}`}>
        {dropdownLink.name}
      </Dropdown.Item>
    );
  });

  return (
    <div className="loginMargin">
      <Dropdown
        text={username}
        icon='user'
        floating
        labeled
        button
        className="icon primary"
      >
        <Dropdown.Menu>
          {linkItems}
          <Dropdown.Item onClick={() => dispatch(logout())}>
            Sign out
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
};

export default LoginDropdown;
