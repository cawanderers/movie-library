import React, { useEffect, useState } from "react";
import tmdb from "../api/tmdb";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import CastCard from "./CastCard";
import CarouselArrowNext from "./CarouselArrowNext";
import CarouselArrowPrev from "./CarouselArrowPrev";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const CastCarousel = ({movieId}: { movieId: number }) => {
  const [cast, setCast] = useState<any[] | undefined>([]);

  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    swipeToSlide: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    nextArrow: <CarouselArrowNext />,
    prevArrow: <CarouselArrowPrev />
  };

  useEffect(() => {
    const getCast = async () => {
      const {data: {cast}} = await tmdb.getMovieCredits(movieId);
      setCast(cast);
    };
    getCast();
  }, [movieId]);

  const TopBilledCast = cast?.filter((el) => el.order < 9);
  const TopBilledCastList = TopBilledCast?.map((cast) => {
    return (
      <CastCard key={cast.id} cast={cast} />
    )
  });

  if (!cast) {
    return <>Loading...</>
  }

  return (
    <div style={{marginBottom: 50}}>
      <h3 className="ui header">Top Billed Cast</h3>
      <Slider {...settings}>
        {TopBilledCastList}
      </Slider>
    </div>
  );
};

export default CastCarousel;
