import React from "react";
import { Menu } from 'semantic-ui-react';
import { TabOptions } from "../utils";

const MyTab = ({
                 options,
                 selected,
                 onSelectedChange
               }: { options: TabOptions[], selected: any, onSelectedChange: any }) => {
  const tabItem = options.map(({label}) => {
    return (
      <Menu.Item
        key={label}
        name={label}
        active={selected === label}
        onClick={() => onSelectedChange(label)}
      />
    );
  });

  return (
    <Menu pointing secondary size='huge'>
      {tabItem}
    </Menu>
  )
};

export default MyTab;
