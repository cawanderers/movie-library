import React from "react";
import { DetailButton } from "./DetailButtons";
import Tippy from "@tippyjs/react";
import "tippy.js/animations/scale-subtle.css";
import "tippy.js/dist/tippy.css";

const ButtonItem = ({button}: { button: DetailButton }) => {
  return (
    <div className="item">
      <Tippy
        content={button.content}
        placement="bottom"
        animation="scale-subtle"
        duration={200}
        arrow={false}
        delay={[75, 0]}
        offset={[0, 1]}
        className="ui pointing blue label"
      >
        <button className="ui circular primary icon button" onClick={button.onClick}>
          <i className={`${button.icon}`} />
        </button>
      </Tippy>
    </div>
  );
};

export default ButtonItem;
