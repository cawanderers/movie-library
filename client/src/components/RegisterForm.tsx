import React, { useState, useEffect, useRef } from 'react';
import { useFormik } from 'formik';
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { clearErrors } from '../actions/errorActions';
import { Container, Header } from "semantic-ui-react";
import { Register, register } from '../actions/authActions';
import PwdReqList from "./PwdReqList";
import InputField from "./InputField";
import { Div, Wrapper } from "./LoginForm";

export enum E_ERROR {
  REGISTER_FAIL = 'REGISTER_FAIL',
  LOGIN_FAIL = 'LOGIN_FAIL'
}

export interface IError {
  id: E_ERROR;
  msg: string | any;
}

export interface IAuth {
  error: IError;
  auth: any;
}

const validate = (values: Register) => {
  const errors: any = {};
  if (!values.email) {
    errors.email = 'Required';
  } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
    errors.email = 'Invalid email address';
  }
  if (!values.password) {
    errors.password = 'Required';
  }
  if (!values.role) {
    errors.role = 'Required';
  }
  if (!values.username) {
    errors.username = 'Required';
  }
  return errors;
}

const RegisterForm = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const error = useSelector(((state: IAuth) => state.error));
  const auth = useSelector(((state: IAuth) => state.auth));
  const [fieldErr, setFieldErr] = useState(null);
  const ref = useRef<HTMLDivElement>(null);
  const formik = useFormik({
    initialValues: {
      email: '',
      password: '',
      role: '',
      username: '',
    },
    validate,
    onSubmit: values => {
      dispatch(register(values));
    },
  });

  useEffect(() => {
    const onBodyClick = ({target}: { target: any }) => {
      if (ref.current && ref.current.contains(target)) {
        dispatch(clearErrors());
      }
    };
    document.body.addEventListener('click', onBodyClick);
  }, [dispatch]);

  useEffect(() => {
    if (error.id === E_ERROR.REGISTER_FAIL) {
      setFieldErr(error.msg)
    }
  }, [error]);

  useEffect(() => {
    if (auth?.isAuthenticated) {
      setFieldErr(null);
      history.goBack();
    }
  }, [auth, history]);

  return (
    <Div>
      <Wrapper>
        <Container style={{margin: 20}}>
          <Header textAlign="center" as="h1">Sign Up</Header>
        </Container>
        <form className="ui form" onSubmit={formik.handleSubmit}>
          <InputField name={'email'} type={'email'} placeholder={'E-mail address'} formik={formik}
                      fieldErr={fieldErr} />
          <div ref={ref} style={{marginBottom: '14px'}}>
            <InputField name={'username'} type={'text'} placeholder={'Username'} formik={formik} fieldErr={fieldErr} />
          </div>
          <InputField name={'password'} type={'password'} placeholder={'Password'} formik={formik}
                      fieldErr={fieldErr} />
          {formik.values.password ? (<div className="ui message">
            <div className="header">
              Password must contain the following:
            </div>
            <PwdReqList passwordValue={formik.values.password} />
          </div>) : null}
          <InputField name={'role'} type={'text'} placeholder={'Type admin or user'} formik={formik}
                      fieldErr={fieldErr} />
          <button className="ui button primary fluid" type="submit">Submit</button>
        </form>
      </Wrapper>
    </Div>
  );
};

export default RegisterForm;
