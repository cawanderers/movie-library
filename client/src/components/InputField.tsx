import React from "react";
import { getLabel } from "../utils";

const InputField = ({
                      name,
                      type,
                      placeholder,
                      formik,
                      fieldErr
                    }: { name: string, type: string, placeholder: string, formik: any, fieldErr: any }) => {
  const error = formik.errors[name];
  const touched = formik.touched[name];
  const label = getLabel(name);

  return (
    <div className={`field ${touched && error ? "error" : ""}`}>
      <label htmlFor={name}>{label}</label>
      <input
        name={name}
        type={type}
        placeholder={placeholder}
        onChange={formik.handleChange}
        onBlur={formik.handleBlur}
        value={formik.values[name]}
      />
      {touched && error ? (
        <div className="ui pointing red basic label">{error}</div>
      ) : null}
      {fieldErr && fieldErr.fieldName === name ?
        <div className="ui box pointing red basic label">{fieldErr.message.msgBody}</div> : null}
    </div>
  );
};

export default InputField;
