import React from "react";
import { getLabel } from "../utils";

const MyTextarea = ({name, formik}: { name: string, formik: any }) => {
  const label = getLabel(name);
  const error = formik.errors[name];
  const touched = formik.touched[name];

  return (
    <div className={`field ${touched && error ? "error" : ""}`}>
      <label htmlFor={name}>{label}</label>
      <textarea rows={7} id={name} name={name} onChange={formik.handleChange}
                value={formik.values[name]} />
      {touched && error && (
        <div className="ui pointing red basic label">{error}</div>
      )}
    </div>
  );
};

export default MyTextarea;
