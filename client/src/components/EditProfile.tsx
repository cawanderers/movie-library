import React, { useEffect } from "react";
import { useFormik } from 'formik';
import InputField from "./InputField";
import SelectInput from "./SelectInput";
import DateInput from "./DateInput";
import MyTextarea from "./MyTextarea";
import { movieGenres } from "../utils";
import { useDispatch, useSelector } from "react-redux";
import { createMyProfile, getMyProfile, updateMyProfile } from "../actions/profileActions";
import { Div, Wrapper } from "./LoginForm";
import { Container, Header } from "semantic-ui-react";
import { useHistory } from "react-router-dom";

const options = [
  {
    label: '-',
    value: '-'
  },
  {
    label: 'Female',
    value: 'Female'
  },
  {
    label: 'Male',
    value: 'Male'
  },
  {
    label: 'Non-binary',
    value: 'Non-binary'
  }
];

const EditProfile = () => {
  const myProfile = useSelector(((state: any) => state.myProfile));
  const dispatch = useDispatch();
  const history = useHistory();

  const formik = useFormik({
    initialValues: {
      firstName: '',
      lastName: '',
      gender: '',
      birthday: '',
      about: '',
      favouriteMovieGenre: ''
    },
    onSubmit: values => {
      if (myProfile?.length) {
        dispatch(updateMyProfile(myProfile[0]._id, values));
      } else {
        dispatch(createMyProfile(values));
      }
      history.goBack();
    }
  });

  useEffect(() => {
    dispatch(getMyProfile());
  }, [dispatch]);

  return (
    <Div>
      <Wrapper>
        <Container>
          <Header textAlign="center" as="h1" dividing>Edit Profile</Header>
        </Container>
        <form className="ui form" onSubmit={formik.handleSubmit}>
          <div className="field">
            <div className="two fields">
              <InputField name={'firstName'} type={'text'} placeholder={'First Name'} formik={formik} fieldErr={null} />
              <InputField name={'lastName'} type={'text'} placeholder={'Last Name'} formik={formik} fieldErr={null} />
            </div>
          </div>
          <div className="two fields">
            <SelectInput name={'gender'} formik={formik} options={options} />
            <DateInput name={'dateOfBirth'} formik={formik} />
          </div>
          <MyTextarea name={'about'} formik={formik} />
          <div className="two fields">
            <SelectInput name={'favouriteMovieGenre'} formik={formik} options={movieGenres} />
          </div>
          <button className="ui primary fluid button" type="submit">
            Save
          </button>
        </form>
      </Wrapper>
    </Div>
  );
};

export default EditProfile;
