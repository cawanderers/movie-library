import React from "react";
import Stars from "react-rating";
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const FontAwesome = styled(FontAwesomeIcon)`
  color: #E09015;
`;

const MovieRating = ({number}: { number: number }) => {
  return (
    <Stars
      emptySymbol={<FontAwesome icon={['far', 'star']} size="1x" />}
      fullSymbol={<FontAwesome icon={['fas', 'star']} size="1x" />}
      initialRating={number}
      readonly
    />
  );
};

export default MovieRating;
