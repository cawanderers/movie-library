import React, { useEffect, useState } from "react";
import queryString from 'query-string';
import Dropdown from "./Dropdown";
import Movies from "./Movies";
import tmdb from "../api/tmdb";
import MyLoader from "./MyLoader";
import Pagination from "./Pagination";

export interface Option {
  label: string,
  value: string
}

const sortOptions: Option[] = [
  {
    label: 'Popular',
    value: 'popular?'
  },
  {
    label: 'Upcoming',
    value: 'upcoming?'
  },
  {
    label: 'Now Playing',
    value: 'now_playing?'
  },
  {
    label: 'Top Rated',
    value: 'top_rated?'
  }
];

const MoviesListByFilter = () => {
  const [filterOption, setFilterOption] = useState(sortOptions[0]);
  const [movies, setMovies] = useState([]);
  const [totalPages, setTotalPages] = useState(1);
  const [page, setPage] = useState(1);
  const [loading, setLoading] = useState(false);

  const params = queryString.parse(window.location.search);

  useEffect(() => {
    setLoading(true);
    const getMovies = async () => {
      const {data: {results, total_pages, page}} = await tmdb.getMoviesByFilter(filterOption.value, params.page);
      setMovies(results);
      setTotalPages(total_pages);
      setPage(page)
    };
    getMovies()
      .then(() => setLoading(false));

    return () => setLoading(true)
  }, [filterOption, params.page]);

  return loading ? <MyLoader /> : (
    <div className="column twelve wide">
      <h1 className='ui header' style={{marginTop: 5}}>Movies List</h1>
      <Dropdown label={'Sorted By'} selected={filterOption} onSelectedChange={setFilterOption}
                options={sortOptions} />
      <Movies movies={movies} />
      <Pagination page={page} totalPages={totalPages} />
    </div>
  );
};

export default MoviesListByFilter;
