import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const CarouselArrowNext = (props: any) => {
  return (
    <FontAwesomeIcon
      style={{
        right: '-30px',
        position: 'absolute',
        top: '50%',
        display: 'block',
        width: '25px',
        height: '25px',
        padding: '0',
        transform: 'translate(0, -50%)',
        cursor: 'pointer',
      }}
      onClick={props.onClick}
      icon='chevron-right'
      size="1x"
    />
  );
};

export default CarouselArrowNext;
