import React from "react";
import { Button } from "semantic-ui-react";
import MySearchBar from "./MySearchBar";
import './MobileAppHeader.scss';

const MobileAppHeader = ({visible, setVisible}: { visible: boolean, setVisible: (value: boolean) => void }) => {

  return (
    <div className="ui grid">
      <div className="four wide column">
        <Button basic size="massive" icon="bars" className='icon' position="right"
                onClick={() => setVisible(!visible)} />
      </div>
      <div className="mobileSearchContainer">
        <MySearchBar />
      </div>
    </div>
  );
};

export default MobileAppHeader;
