import React, { useEffect, useState } from "react";
import tmdb from "../api/tmdb";
import { Item } from "semantic-ui-react";
import './MySearchBar.scss';
import { Redirect } from "react-router-dom";
import SearchSuggestionItem from "./SearchSuggestionItem";

const MySearchBar = () => {
  const [term, setTerm] = useState<string>('');
  const [redirect, setRedirect] = useState('');
  const [debouncedTerm, SetDebouncedTerm] = useState(term);
  const [results, setResults] = useState<any[]>([]);
  const [showSuggestions, setShowSuggestions] = useState(false);
  const [activeIndex, setActiveIndex] = useState(0);

  useEffect(() => {
    setResults([]);
    const timerId = setTimeout(() => {
      SetDebouncedTerm(term);
    }, 300);

    return () => {
      clearTimeout(timerId);
    };

  }, [term]);

  useEffect(() => {
    const searchMovies = async () => {
      const {data: {results}} = await tmdb.getMoviesBySearch(debouncedTerm);
      setResults(results);
    };
    searchMovies()

  }, [debouncedTerm]);

  const options = results?.slice(0, 5).map((option: any) => {
    return {...option, 'number': option.id}
  });

  const suggestions = options?.map((option: any, index) => {
    return (
      <SearchSuggestionItem option={option} index={index} activeIndex={activeIndex} />
    )
  });

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTerm(e.target.value);
    setShowSuggestions(true);
    setActiveIndex(0);
  };

  const onSubmit = () => {
    setRedirect(`/movie/${options[activeIndex].number}`)
  };

  const onKeyDown = (e: any) => {
    //User presses ENTER key
    if (e.keyCode === 13) {
      setShowSuggestions(false);
      onSubmit();
      //  User presses Up Arrow key
    } else if (e.keyCode === 38) {
      if (activeIndex === 0) {
        return;
      }
      setActiveIndex(activeIndex - 1);
      //  User presses Down Arrow key
    } else if (e.keyCode === 40) {
      if (activeIndex === options?.length - 1) {
        return;
      }
      setActiveIndex(activeIndex + 1);
    }
  };

  return (
    <>
      {redirect && <Redirect push to={redirect} />}
      <form className="ui form" onSubmit={onSubmit}>
        <div className="ui centered grid">
          <div className="ui action input searchBarContainer">
            <input type="text" onChange={onChange} onKeyDown={onKeyDown} value={term} placeholder="Search..." />
            <button className="ui icon button" type="submit">
              <i className="search icon" />
            </button>
          </div>
          {showSuggestions && term ?
            <Item.Group className={'suggestionContainer'}>
              {suggestions}
            </Item.Group> : null
          }
        </div>
      </form>
    </>
  );
};

export default MySearchBar;
