import React from "react";
import { Item } from "semantic-ui-react";
import { imageSrc } from "../utils";

const SearchSuggestionItem = ({option, index, activeIndex}: { option: any, index: number, activeIndex: number }) => {
  return (
    <Item className={`${index === activeIndex ? 'suggestionActive' : ''}`} key={option.id} as='a'
          href={`/movie/${option.number}`}>
      <Item.Image size='tiny' src={`${imageSrc}${option.poster_path}`} />
      <Item.Content verticalAlign='middle'>
        <Item.Header>{option.title}</Item.Header>
      </Item.Content>
    </Item>
  );
};

export default SearchSuggestionItem;
