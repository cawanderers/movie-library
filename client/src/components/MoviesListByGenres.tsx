import React, { useEffect, useState } from "react";
import Movies from "./Movies";
import tmdb from "../api/tmdb";
import { Option } from "./MoviesListByFilter";
import Dropdown from "./Dropdown";
import MyLoader from "./MyLoader";

const sortOptions: Option[] = [
  {
    label: 'Popularity',
    value: 'popularity.desc'
  },
  {
    label: 'Top Revenue',
    value: 'revenue.desc'
  },
  {
    label: 'Top Average',
    value: 'vote_average.desc'
  }
];

const MoviesListByGenres = ({genres}: { genres: any[] }) => {
  const [filterOption, setFilterOption] = useState(sortOptions[0]);
  const [movies, setMovies] = useState([]);
  const [loading, setLoading] = useState(false);
  const genresList = genres?.map(({id}: { id: number }) => id).join(',');

  useEffect(() => {
    setLoading(true);
    const getMovies = async () => {
      const {data: {results}} = await tmdb.getMoviesByGenres(genresList, filterOption.value);
      setMovies(results);
    };
    getMovies()
      .then(() => setLoading(false));

    return () => setLoading(true)
  }, [genresList, filterOption.value]);

  return loading ? <MyLoader /> : (
    <div className="column twelve wide">
      <h1 className='ui header' style={{marginTop: 5}}>Movies List</h1>
      <Dropdown label={'Sorted By'} selected={filterOption} onSelectedChange={setFilterOption}
                options={sortOptions} />
      <Movies movies={movies} />
    </div>
  );
};

export default MoviesListByGenres;
