import React from "react";
import styled from 'styled-components';
import { imageSrc } from "../utils";

const MoviePoster = styled.img`
  margin-top: 20px;
  height: 450px;
  width: 300px;
  border-radius: 6px;
`;

const Poster = ({url, id}: { url: string, id: number }) => {
  return (
    <MoviePoster src={`${imageSrc}${url}`}
                 alt={String(id)} />
  );
};

export default Poster;
