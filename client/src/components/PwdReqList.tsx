import React from "react";
import ListItem from "./ListItem";

const validationRules = [
  {
    matcher: /[a-z]/g,
    content: 'at least one lowercase letter'
  },
  {
    matcher: /[A-Z]/g,
    content: 'at least one uppercase letter'
  },
  {
    matcher: /[0-9]/g,
    content: 'at least one number'
  },
  {
    matcher: /[!@#$%^&*(),.?":{}|<>]/g,
    content: 'at least one special character(for example: !, $, #, ?, %, ...)'
  },
  {
    matcher:/[a-zA-Z0-9]{8,}/g,
    content: 'minimum 8 characters'
  }
];

const PwdReqList = ({ passwordValue }: { passwordValue: string }) => {
  const renderedList = validationRules.map(({matcher, content}) => {
    return <ListItem key={content} fieldValue={passwordValue} matcher={matcher} content={content} />;
});
  return <div className="ui list">
    {renderedList}
  </div>
};

export default PwdReqList;
