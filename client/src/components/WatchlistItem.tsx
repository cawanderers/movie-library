import React from "react";
import { removeFromWatchlist } from "../actions/collectedMoviesActions";
import { Button, Item } from "semantic-ui-react";
import { imageSrc } from "../utils";
import MovieRating from "./MovieRating";
import { useDispatch } from "react-redux";
import { WatchlistMovie } from "./Watchlist";

const WatchlistItem = (collectedMovie: WatchlistMovie) => {
  const dispatch = useDispatch();
  const onClick = () => {
    dispatch(removeFromWatchlist(collectedMovie._id));
  };

  const movieScore = () => {
    return (
      <MovieRating number={collectedMovie.rating / 2} />
    );
  };

  return (
    <Item>
      <Item.Image as='a' href={`/movie/${collectedMovie.movieId}`} size='tiny' src={`${imageSrc}${collectedMovie.poster}`} />
      <Item.Content>
        <Item.Header as='a' href={`/movie/${collectedMovie.movieId}`}>{collectedMovie.title}</Item.Header>
        <Item.Meta>
          <span>{movieScore()}</span>
          <span>Release Date: {collectedMovie.releaseDate}</span>
        </Item.Meta>
        <Item.Description>
          <p>{collectedMovie.overview}</p>
        </Item.Description>
        <Item.Extra>
          <Button primary floated='right' content='Remove' icon='remove' labelPosition='right' onClick={onClick} />
        </Item.Extra>
      </Item.Content>
    </Item>
  );
};

export default WatchlistItem;
