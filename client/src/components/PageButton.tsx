import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { Button } from "semantic-ui-react";

const WrapperLink = styled(Link)`
  text-decoration: none;
`;

const PageButton = ({page, icon, position}: { page: number, icon: string, position: "right" | "left" | undefined }) => {
  return (
    <WrapperLink
      to={`${process.env.PUBLIC_URL}?page=${page}`}
    >
      <Button primary content={`Page ${page}`} icon={icon} labelPosition={position} />
    </WrapperLink>
  );
};

export default PageButton;
