import React, { useEffect, useState } from "react";
import tmdb from "../api/tmdb";
import Movies from "./Movies";
import { Element, animateScroll as scroll } from 'react-scroll';
import MyLoader from "./MyLoader";

const initialMoviesNumber = 4;

const MoviesByTab = ({selected, movieId}: { selected: string, movieId: number }) => {
  const [movies, setMovies] = useState<any[] | undefined>([]);
  const [loadedMovies, setLoadedMovies] = useState<number>(initialMoviesNumber);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    scroll.scrollToTop({
      smooth: true,
      delay: 200,
    });

    const getMovies = async () => {
      const {data: {results}} = await tmdb.getMoviesByTab(movieId, selected.toLowerCase());
      setMovies(results);
    };
    getMovies()
      .then(() => setLoading(false));

    return () => setLoading(true)
  }, [movieId, selected]);

  const loadMore = () => {
    setLoadedMovies(preLoadedMovies => preLoadedMovies + 16);
  };

  const visibleMovies = movies?.slice(0, loadedMovies);

  return loading ? <MyLoader /> : (
    <>
      <Element name="scroll-to-element">
        <Movies movies={visibleMovies} />
      </Element>
      {loadedMovies === 20 ? null : <button className="fluid ui primary button" onClick={loadMore}>Load More</button>}
    </>
  );
};

export default MoviesByTab;
