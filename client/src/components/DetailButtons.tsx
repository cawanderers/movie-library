import React, { useEffect, useState } from "react";
import ButtonItem from "./ButtonItem";
import {
  addToWatchlist,
  CollectedMovie,
  getMovieFromWatchlist,
  removeFromWatchlist
} from "../actions/collectedMoviesActions";
import { useDispatch, useSelector } from "react-redux";
import {
  getMyFavoriteMovie,
  markAsMyFavoriteMovie,
  unmarkAsMyFavoriteMovie,
  updateMyFavoriteMovie
} from "../actions/FavoriteMovieActions";
import { sendGAEvent } from "../utils";

interface OnClickFunc {
  (): void;
}

export interface DetailButton {
  icon: string,
  content: string,
  onClick?: OnClickFunc
}

const DetailButtons = ({movieId, title, poster, rating, overview, releaseDate}: CollectedMovie) => {
  const collectedMovies = useSelector(((state: any) => state.collectedMovies));
  const favoriteMovie = useSelector(((state: any) => state.favoriteMovie));
  const [movieTitle, setMovieTitle] = useState('');
  const [favoriteMovieTitle, setFavoriteMovieTitle] = useState('');
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getMovieFromWatchlist(movieId));
    dispatch(getMyFavoriteMovie());
  }, [dispatch, movieId]);

  const length = collectedMovies?.length;
  const favoriteMovieLength = favoriteMovie?.length;

  useEffect(() => {
    if (length) {
      setMovieTitle(collectedMovies[0].title);
    } else {
      setMovieTitle('');
    }
  }, [length, collectedMovies]);

  useEffect(() => {
    if (favoriteMovieLength) {
      setFavoriteMovieTitle(favoriteMovie[0].title);
    } else {
      setFavoriteMovieTitle('');
    }
  }, [favoriteMovieLength, favoriteMovie]);

  const addToWatchlistOnClick = () => {
    if (title === movieTitle) {
      dispatch(removeFromWatchlist(collectedMovies[0]._id));
      setMovieTitle('');
      sendGAEvent(`Removed ${title} from watchlist`);
    } else {
      dispatch(addToWatchlist({movieId, title, poster, rating, overview, releaseDate}));
      sendGAEvent(`Added ${title} to watchlist`);
    }
  };

  const markAsFavoriteOnClick = () => {
    if (favoriteMovieTitle === title) {
      dispatch(unmarkAsMyFavoriteMovie(favoriteMovie[0]._id));
      sendGAEvent(`Unmarked ${title} as favorite`);
      setFavoriteMovieTitle('');
    } else if (favoriteMovieLength) {
      dispatch(updateMyFavoriteMovie(favoriteMovie[0]._id, {movieId, title, poster, rating, overview, releaseDate}));
      sendGAEvent(`Marked ${title} as favorite`);
    } else {
      dispatch(markAsMyFavoriteMovie({movieId, title, poster, rating, overview, releaseDate}));
      sendGAEvent(`Marked ${title} as favorite`);
    }
  };

  const buttons = [
    {
      icon: 'file outline icon',
      content: 'Add to list',
      onClick: undefined
    },
    {
      icon: favoriteMovieTitle === title ? 'red heart icon' : 'heart icon',
      content: favoriteMovieTitle === title ? 'Unmark as favorite' : 'Mark as favorite',
      onClick: markAsFavoriteOnClick
    },
    {
      icon: movieTitle === title ? 'yellow bookmark icon' : 'bookmark icon',
      content: movieTitle === title ? 'Remove from watchlist' : 'Add to watchlist',
      onClick: addToWatchlistOnClick
    },
    {
      icon: 'star outline icon',
      content: 'Rate it',
      onClick: undefined
    }
  ]

  const buttonList = buttons.map((button: DetailButton) => {
    return (
      <ButtonItem key={button.content} button={button} />
    )
  });

  return (
    <div className="ui horizontal list" style={{marginRight: 'auto'}}>
      {buttonList}
    </div>
  );
};

export default DetailButtons;
