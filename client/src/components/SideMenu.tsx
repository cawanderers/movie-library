import React, { useState } from "react";
import MyMenuItem from "./MyMenuItem";
import { url } from "../contants";
import { useLocation } from "react-router-dom";
import { isCurrentPath, pagePaths } from "../utils";
import { Menu } from "semantic-ui-react";

export interface MenuOption {
  label: string,
  link: string,
  icon: string
}

export const menuOptions = [
  {
    label: 'Home',
    link: `${url}/filter/Popular`,
    icon: "home icon"
  },
  {
    label: 'Movies by Genres',
    link: `${url}/discover/Genres/Popularity`,
    icon: "compass outline icon"
  }
];

const SideMenu = () => {
  const [activeItem, setActiveItem] = useState('');
  const location = useLocation();

  const menuContent = menuOptions.map((menuOption: MenuOption) => {
    return (
      <MyMenuItem key={menuOption.label} option={menuOption} selected={activeItem} onSelectedChange={setActiveItem} />
    )
  });

  return isCurrentPath(location.pathname, pagePaths) ? null : (
    <Menu secondary vertical>
      {menuContent}
    </Menu>
  );
};


export default SideMenu;
