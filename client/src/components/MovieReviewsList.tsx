import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMovieComments } from "../actions/movieCommentsActions";
import { Feed, Header } from "semantic-ui-react";
import MovieReviewsItem from "./MovieReviewsItem";
import './MovieReviewsList.scss';
import ReviewFormModal from "./ReviewFormModal";

export interface PostedBy {
  _id: string;
  username: string;
}

export interface Review {
  content: string;
  createdAt: string;
  postedBy: PostedBy;
  _id: string;
}

const MovieReviewsList = ({
                            movieId,
                            movieTitle,
                          }: { movieId: number, movieTitle: string }) => {
    const comments = useSelector(((state: any) => state.comments));
    const dispatch = useDispatch();

    useEffect(() => {
      dispatch(getMovieComments(movieId));
    }, [dispatch, movieId]);

    const commentsList = () => {
      return comments?.length ? (
        comments.map((comment: Review) => (
          <MovieReviewsItem key={comment._id} content={comment.content} createdAt={comment.createdAt}
                            postedBy={comment.postedBy} />
        ))) : <>Would you like to be the first to review <b>{movieTitle}</b>?</>
    };

    return (
      <Feed className="movieReviews">
        <Header as='h3' dividing>
          User reviews
          <ReviewFormModal movieId={movieId} movieTitle={movieTitle}/>
        </Header>
        {commentsList()}
      </Feed>
    );
  }
;

export default MovieReviewsList;
