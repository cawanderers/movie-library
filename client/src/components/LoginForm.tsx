import React, { useState, useEffect } from 'react';
import { useFormik } from 'formik';
import { useDispatch, useSelector } from "react-redux";
import { login, Login } from "../actions/authActions";
import { E_ERROR, IAuth } from "./RegisterForm";
import { Container, Header } from "semantic-ui-react";
import InputField from "./InputField";
import { useHistory, Link } from "react-router-dom";
import styled from "styled-components";
import './LoginForm.scss';

export const Div = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  min-height: 100vh;
`;

export const Wrapper = styled.div`
  width: 80%;
  max-width: 500px;
`;

const validate = (values: Login) => {
  const errors: any = {};
  if (!values.username) {
    errors.username = 'Required';
  }
  if (!values.password) {
    errors.password = 'Required';
  }
  return errors;
};

const LoginForm = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const error = useSelector(((state: IAuth) => state.error));
  const [fieldErr, setFieldErr] = useState(null);
  const auth = useSelector(((state: IAuth) => state.auth));
  const formik = useFormik({
    initialValues: {
      password: '',
      username: '',
    },
    validate,
    onSubmit: values => {
      dispatch(login(values));
    },
  });

  useEffect(() => {
    if (error.id === E_ERROR.LOGIN_FAIL) {
      setFieldErr(error.msg)
    }
  }, [error]);

  useEffect(() => {
    if (auth?.isAuthenticated) {
      setFieldErr(null);
      history.goBack();
    }
  }, [auth, history]);

  return (
    <Div>
      <Wrapper>
        <Container>
          <Header textAlign="center" as="h1">Sign In</Header>
        </Container>
        {fieldErr ?
          <div className="ui container center aligned large red basic label">Invalid Username and/or
            Password</div> : null}
        <form className="ui form" onSubmit={formik.handleSubmit}>
          <InputField name={'username'} type={'text'} placeholder={'Username'} formik={formik} fieldErr={fieldErr} />
          <InputField name={'password'} type={'password'} placeholder={'Password'} formik={formik}
                      fieldErr={fieldErr} />
          <button className="ui primary fluid button" type="submit">Sign In</button>
        </form>
        <Link to={'/register'}>
          <Header className="createAccountContainer" as='h5' textAlign='center'>
            Create Account
          </Header>
        </Link>
      </Wrapper>
    </Div>
  );
};

export default LoginForm;
