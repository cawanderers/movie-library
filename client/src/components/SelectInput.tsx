import React from "react";
import { getLabel } from "../utils";

const SelectInput = ({name, formik, options}: { name: string, formik: any, options: any }) => {
  const optionItems = options.map(({value, label}: { value: string, label: string }) => {
    return <option key={label} value={value} label={label} />
  });
  const label = getLabel(name);

  return (
    <div className="field">
      <label>{label}</label>
      <select className="ui fluid dropdown"
              name={name}
              value={formik.values[name]}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              style={{marginBottom: '14px'}}
      >
        {optionItems}
      </select>
    </div>
  );
};

export default SelectInput;
