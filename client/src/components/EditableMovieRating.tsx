import React, { useEffect } from "react";
import Stars from "react-rating";
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useDispatch, useSelector } from "react-redux";
import { createMovieRating, getMyMovieRating, updateMyMovieRating } from "../actions/movieRatingsActions";
import { Label } from "semantic-ui-react";
import { sendGAEvent } from "../utils";

const FontAwesome = styled(FontAwesomeIcon)`
  color: #E09015;
`;

const StyledStars = styled(Stars)`
  margin-bottom: 15px;
`

const emptySymbol = <FontAwesome icon={['far', 'star']} size="1x" />;
const fullSymbol = <FontAwesome icon={['fas', 'star']} size="1x" />;

const MovieRating = ({movieScore, movieId, movieTitle}: { movieScore: number, movieId: number, movieTitle: string }) => {
  const dispatch = useDispatch();
  const ratings = useSelector(((state: any) => state.ratings));

  useEffect(() => {
    dispatch(getMyMovieRating(movieId));
  }, [dispatch, movieId]);

  const onClick = (rate: number) => {
    sendGAEvent(`Rate movie ${movieTitle} as ${movieScore}`);
    if (ratings?.length) {
      dispatch(updateMyMovieRating(ratings[0]._id, {rate, movieId}));
    } else {
      dispatch(createMovieRating({rate, movieId}));
    }
  };

  const ratingLabel = () => {
    if (ratings?.length) {
      return (
        <Label basic color='blue' pointing='left'>
          Your rating is {ratings[0].rate} / 10
        </Label>
      );
    } else {
      return;
    }
  };

  return (
    <>
      <StyledStars
        emptySymbol={emptySymbol}
        fullSymbol={fullSymbol}
        initialRating={movieScore}
        fractions={2}
        onClick={onClick}
        stop={10}
      />
      {ratingLabel()}
    </>
  );
};

export default MovieRating;
