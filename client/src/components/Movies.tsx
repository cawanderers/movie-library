import React from "react";
import MoviesListCard from "./MoviesListCard";

interface Movie {
  title: string,
  poster_path: string,
  vote_average: number,
  id: number
}

const Movies = ({movies}: { movies: any }) => {
  const movieCards: Movie[] = movies.map(({
                                            title,
                                            poster_path,
                                            vote_average,
                                            id
                                          }: { title: string, poster_path: string, vote_average: number, id: number }) => {
    return (
      <MoviesListCard key={id} movieId={id} title={title} posterUrl={poster_path} rate={vote_average} />
    )
  });

  return (
    <div className="ui four column grid">
      {movieCards}
    </div>
  );
};

export default Movies;
