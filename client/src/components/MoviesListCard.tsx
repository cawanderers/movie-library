import React, { useEffect, useState } from "react";
import MovieRating from "./MovieRating";
import { Image } from 'semantic-ui-react'
import styled from "styled-components";
import { Link } from "react-router-dom";
import LazyLoad from "react-lazyload";
import { imageSrc } from "../utils";
import './MoviesListCard.scss';

const loadingPic = `${process.env.PUBLIC_URL}/assets/images/Rolling-1s-200px.svg`

const StyledLink = styled(Link)`
  color: black;
  text-align: center;
  text-decoration: none;
  display: block;
`;

const StyledImg = styled.img`
  height: 228.28px !important;
  width: 152.19px;
`

const MoviesListCard = ({
                          movieId,
                          title,
                          posterUrl,
                          rate
                        }: { movieId: number, title: string, posterUrl: string, rate: number }) => {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    return () => setLoaded(false);
  }, []);

  return (
    <div key={title} className="column">
      <LazyLoad height={200} offset={200}>
        <div className="ui card">
          <div className="image">
            {!loaded ? (
              <StyledImg src={`${loadingPic}`} alt='loadingPic'/>
            ) : null}
            <Image
              as='a'
              className="movieImageContainer"
              href={`/movie/${movieId}`}
              onLoad={() => setLoaded(true)}
              style={loaded ? {} : {display: 'none'}}
              src={`${imageSrc}${posterUrl}`} alt='poster'
            />
          </div>
        </div>
        <div style={{alignItems: 'center', height: 71}}>
          <StyledLink to={`${process.env.PUBLIC_URL}/movie/${movieId}`}>
            <h3 className="ui header" style={{textAlign: 'center'}}>{title}</h3>
          </StyledLink>
          <div style={{textAlign: 'center'}}>
            <MovieRating number={rate / 2}/>
            <span style={{marginLeft: 6, color: '#E09015'}}>{rate}</span>
          </div>
        </div>
      </LazyLoad>
    </div>
  );
};

export default MoviesListCard;
