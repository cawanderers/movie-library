import { IAction } from "./authReducer";
import { IFavoriteMovie } from "../../../models/FavoriteMovie";

const initialState = {
  favoriteMovie: []
};

interface IState {
  favoriteMovie: IFavoriteMovie[];
}

const favoriteMovieReducer = (favoriteMovie: IState = initialState, action: IAction) => {
  switch (action.type) {
    case 'GET_MY_FAVORITE_MOVIE':
    case 'MARK_AS_MY_FAVORITE_MOVIE':
    case 'UNMARK_AS_MY_FAVORITE_MOVIE':
    case 'UPDATE_MY_FAVORITE_MOVIE':
      return action.payload.myFavoriteMovie;
    case 'GET_MY_FAVORITE_MOVIE_FAIL':
    case 'MARK_AS_MY_FAVORITE_MOVIE_FAIL':
    case 'UNMARK_AS_MY_FAVORITE_MOVIE_FAIL':
    case 'UPDATE_MY_FAVORITE_MOVIE_FAIL':
    default:
      return favoriteMovie;
  }
};

export default favoriteMovieReducer;
