export interface IAction {
  type: string;
  payload?: any;
}

const authReducer = (state = {}, action: IAction) => {
  switch (action.type) {
    case 'AUTHENTICATED_SUCCESS':
    case 'LOGIN_SUCCESS':
    case 'REGISTER_SUCCESS':
      return {
        ...state,
        ...action.payload,
        isAuthenticated: true
      };
    case 'AUTHENTICATED_FAIL':
    case 'LOGIN_FAIL':
    case 'LOG_OUT':
    case 'REGISTER_FAIL':
      return {
        ...state,
        ...action.payload,
        isAuthenticated: false
      };
    default:
      return state;
  }
};

export default authReducer;
