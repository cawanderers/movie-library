import { IAction } from "./authReducer";
import { IRating } from "../../../models/Rating";

const initialState = {
  ratings: []
};

interface IState {
  ratings: IRating[];
}

const movieRatingsReducer = (ratings: IState = initialState, action: IAction) => {
  switch (action.type) {
    case 'CREATE_MOVIE_RATING':
    case 'GET_MOVIE_RATINGS':
      return [...action.payload.ratings];
    case 'GET_MY_MOVIE_RATING':
      return action.payload.myRating;
    case 'GET_MY_MOVIES_RATINGS':
      return action.payload.myRatings;
    case 'UPDATE_MY_MOVIE_RATING':
      return action.payload;
    case 'CREATE_MOVIE_RATING_FAIL':
    case 'GET_MOVIE_RATINGS_FAIL':
    case 'GET_MY_MOVIE_RATING_FAIL':
    case 'GET_MY_MOVIES_RATINGS_FAIL':
    case 'UPDATE_MY_MOVIE_RATING_FAIL':
    default:
      return ratings;
  }
};

export default movieRatingsReducer;
