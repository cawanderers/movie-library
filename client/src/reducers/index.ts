import { combineReducers } from "redux";
import authReducer from "./authReducer";
import errorReducer from "./errorReducer";
import movieCommentsReducer from "./movieCommentsReducer";
import movieRatingsReducer from "./movieRatingsReducer";
import collectedMoviesReducer from "./collectedMoviesReducer";
import favoriteMovieReducer from "./favoriteMovieReducer";
import profileReducer from "./profileReducer";

export default combineReducers({
  auth: authReducer,
  comments: movieCommentsReducer,
  error: errorReducer,
  ratings: movieRatingsReducer,
  collectedMovies: collectedMoviesReducer,
  favoriteMovie: favoriteMovieReducer,
  myProfile: profileReducer
});
