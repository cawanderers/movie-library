import { IAction } from "./authReducer";
import { IComment } from "../../../models/Comment";

const initialState = {
  comments: []
};

interface IState {
  comments: IComment[];
}

const movieCommentsReducer = (comments: IState = initialState, action: IAction) => {
  switch (action.type) {
    case 'CREATE_MOVIE_COMMENT':
    case 'GET_MOVIE_COMMENTS':
      return [...action.payload.comments];
    case'CREATE_MOVIE_COMMENTS_FAIL':
    case 'GET_MOVIE_COMMENTS_FAIL':
      return {
        ...comments
      }
    default:
      return comments;
  }
};

export default movieCommentsReducer;
