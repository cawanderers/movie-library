import { IAction } from "./authReducer";
import { IProfile } from "../../../models/Profile";

const initialState = {
  myProfile: []
};

interface IState {
  myProfile: IProfile[];
}

const profileReducer = (myProfile: IState = initialState, action: IAction) => {
  switch (action.type) {
    case 'CREATE_MY_PROFILE':
    case 'GET_MY_PROFILE':
    case 'UPDATE_MY_PROFILE':
      return action.payload.myProfile;
    case 'CREATE_MY_PROFILE_FAIL':
    case 'GET_MY_PROFILE_FAIL':
    case 'UPDATE_MY_PROFILE_ FAIL':
    default:
      return myProfile;
  }
};

export default profileReducer;
