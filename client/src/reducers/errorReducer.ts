import { IAction } from "./authReducer";
import { ErrPayload } from "../actions/errorActions";

const initialState = {
  msg: {},
  id: ''
}

const errorReducer = (state = initialState, action: IAction): ErrPayload => {
  switch (action.type) {
    case 'GET_ERRORS':
      return action.payload;
    case 'CLEAR_ERRORS':
      return {
        msg: {},
        id: ''
      };
    default:
      return state;
  }
};

export default errorReducer;
