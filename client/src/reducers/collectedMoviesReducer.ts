import { IAction } from "./authReducer";
import { ICollectedMovie } from "../../../models/CollectedMovie";

const initialState = {
  collectedMovies: []
};

interface IState {
  collectedMovies: ICollectedMovie[];
}

const collectedMoviesReducer = (collectedMovies: IState = initialState, action: IAction) => {
  switch (action.type) {
    case 'ADD_TO_WATCHLIST':
    case 'GET_MOVIE_FROM_WATCHLIST_BY_MOVIE_ID':
      return action.payload.movie;
    case 'GET_MY_WATCHLIST':
    case 'REMOVE_MOVIE_FROM_WATCHLIST':
      return [...action.payload.myCollectedMovies];
    case 'ADD_TO_WATCHLIST_FAIL':
    case 'GET_MY_WATCHLIST_FAIL':
    case 'GET_MOVIE_FROM_WATCHLIST_BY_MOVIE_ID_FAIL':
    case 'REMOVE_MOVIE_FROM_WATCHLIST_FAIL':
    default:
      return collectedMovies;
  }
};

export default collectedMoviesReducer;
