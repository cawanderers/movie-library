import axios from "axios";

const tmdb = {
  getMoviesByFilter(filterBy: string, page: string | string[] | null): any {
    return apiBase.get(`/movie/${filterBy}&language=en-US`, {
      params: {
        page
      }
    });
  },
  getMoviesByGenres(genres: string, option: string): any {
    return apiBase.get('/discover/movie', {
      params: {
        with_genres: genres,
        sort_by: option
      }
    });
  },
  getSingleMovie(id: number): any {
    return apiBase.get(`/movie/${id}`);
  },
  getMovieTrailer(id: number): any {
    return apiBase.get(`/movie/${id}/videos`);
  },
  getMovieCredits(id: number): any {
    return apiBase.get(`/movie/${id}/credits`);
  },
  getMoviesByTab(id: number, option: string): any {
    return apiBase.get(`/movie/${id}/${option}`);
  },
  getMovieImages(id: number): any {
    return apiBase.get(`/movie/${id}/images`);
  },
  getMoviesBySearch(query: string): any {
    return apiBase.get('/search/movie', {
      params: {
        query: query
      }
    });
  }
};

const apiBase = axios.create({
  baseURL: 'https://api.themoviedb.org/3',
  params: {
    api_key: '60c8970f3570f7500258061d94cdca11'
  }
});

export default tmdb;
