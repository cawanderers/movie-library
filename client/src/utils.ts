import moment from "moment";
import ReactGa from "react-ga";

interface Path {
  pathname: string;
}

export const genresPaths: Path[] = [
  {
    pathname: '/discover/Genres/Popularity'
  },
  {
    pathname: '/discover/Genres/Top Revenue'
  },
  {
    pathname: '/discover/Genres/Top Average'
  }
];

export const pagePaths: Path[] = [
  {
    pathname: '/login'
  },
  {
    pathname: '/register'
  },
  {
    pathname: '/user/profile'
  }
];

export function isCurrentPath(path: string, paths: Path[]) {
  return paths.some(el => el.pathname === path);
}

export function getLabel(name: string) {
  return (name.charAt(0).toUpperCase() + name.slice(1)).replace(/([A-Z])/g, " $1");
}

export function splitYear(date: string) {
  if (!date) {
    return;
  }
  const [year] = date.split('-');
  return year;
}

export function getMovieInfo(languages: any, time: number, year: any) {
  const info = [];
  if (languages?.length > 0) {
    info.push(languages[0].english_name);
  }
  info.push(time, year);
  return info
    .filter((el) => el !== null)
    .map((el) => (typeof el === 'number' ? `${el} min.` : el))
    .map((el, i, array) => (i !== array.length - 1 ? `${el} / ` : el));
}

export interface Genres {
  label: string,
  value: string,
  id?: number
}

export const movieGenres: Genres[] = [
  {
    label: '-',
    value: '-',
    id: undefined
  },
  {
    label: 'Action',
    value: 'Action',
    id: 28
  },
  {
    label: 'Adventure',
    value: 'Adventure',
    id: 12
  },
  {
    label: 'Animation',
    value: 'Animation',
    id: 16
  },
  {
    label: 'Comedy',
    value: 'Comedy',
    id: 35
  },
  {
    label: 'Crime',
    value: 'Crime',
    id: 80
  },
  {
    label: 'Documentary',
    value: 'Documentary',
    id: 99
  },
  {
    label: 'Drama',
    value: 'Drama',
    id: 18
  },
  {
    label: 'Family',
    value: 'Family',
    id: 10751
  },
  {
    label: 'Fantasy',
    value: 'Fantasy',
    id: 14
  },
  {
    label: 'History',
    value: 'History',
    id: 36
  },
  {
    label: 'Horror',
    value: 'Horror',
    id: 27
  },
  {
    label: 'Music',
    value: 'Music',
    id: 10402
  },
  {
    label: 'Mystery',
    value: 'Mystery',
    id: 9648
  },
  {
    label: 'Romance',
    value: 'Romance',
    id: 10749
  },
  {
    label: 'Science Fiction',
    value: 'Science Fiction',
    id: 878
  },
  {
    label: 'TV Movie',
    value: 'TV Movie',
    id: 10770
  },
  {
    label: 'Thriller',
    value: 'Thriller',
    id: 53
  },
  {
    label: 'War',
    value: 'War',
    id: 10752
  },
  {
    label: 'Western',
    value: 'Western',
    id: 37
  }
];

export interface TabOptions {
  label: string
}

export const movieTabOptions: TabOptions[] = [
  {
    label: 'Recommendations'
  },
  {
    label: 'Similar'
  }
];

export const movieImagesTabOptions: TabOptions[] = [
  {
    label: 'Backdrops'
  },
  {
    label: 'Posters'
  }
];

export const imageSrc = 'https://image.tmdb.org/t/p/w500';

export function getTimeFromNow(date: string) {
  return moment(date).startOf('hour').fromNow();
}

export function sendGAEvent(action: string) {
  ReactGa.event({
    category: 'Button',
    action: `${action}`
  });
}
