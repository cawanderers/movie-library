import Mongoose, { Schema, model, Model, Document } from 'mongoose';

const {ObjectId} = Mongoose.Schema.Types;

export interface ICollectedMovie extends Document {
  id: string;
  movieId: number;
  title: string;
  poster: string;
  rating: number;
  overview: string;
  releaseDate: string;
}

const CollectedMovieSchema: Schema = new Schema({
  id: {
    type: String
  },
  movieId: {
    type: Number
  },
  title: {
    type: String
  },
  poster: {
    type: String
  },
  rating: {
    type: Number
  },
  overview: {
    type: String
  },
  releaseDate: {
    type: String
  },
  collectedBy: {
    type: ObjectId,
    ref: 'User'
  }
});

export const CollectedMovie: Model<ICollectedMovie> = model('CollectedMovie', CollectedMovieSchema);
