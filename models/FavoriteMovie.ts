import Mongoose, { Schema, model, Model, Document } from 'mongoose';

const {ObjectId} = Mongoose.Schema.Types;

export interface IFavoriteMovie extends Document {
  id: string;
  movieId: number;
  title: string;
  poster: string;
  rating: number;
  overview: string;
  releaseDate: string;
}

const FavoriteMovieSchema: Schema = new Schema({
  id: {
    type: String
  },
  movieId: {
    type: Number
  },
  title: {
    type: String
  },
  poster: {
    type: String
  },
  rating: {
    type: Number
  },
  overview: {
    type: String
  },
  releaseDate: {
    type: String
  },
  markedBy: {
    type: ObjectId,
    ref: 'User'
  }
});

export const FavoriteMovie: Model<IFavoriteMovie> = model('FavoriteMovie', FavoriteMovieSchema);
