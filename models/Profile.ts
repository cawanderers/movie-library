import Mongoose, { Schema, model, Model, Document } from 'mongoose';

const {ObjectId} = Mongoose.Schema.Types;

export interface IProfile extends Document {
  firstName: string,
  lastName: string,
  gender: string,
  birthday: string,
  about: string,
  favouriteMovieGenre: string
}

const ProfileSchema: Schema = new Schema({
  firstName: {
    type: String
  },
  lastName: {
    type: String
  },
  gender: {
    type: String
  },
  birthday: {
    type: String
  },
  about: {
    type: String
  },
  favouriteMovieGenre: {
    type: String
  },
  editedBy: {
    type: ObjectId,
    ref: 'User'
  }
});

export const Profile: Model<IProfile> = model('Profile', ProfileSchema);
