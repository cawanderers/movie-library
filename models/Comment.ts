import Mongoose, { Schema, model, Model, Document } from 'mongoose';

const {ObjectId} = Mongoose.Schema.Types;

interface CommentLike {
  type: string;
  ref: "User"
}

export interface IComment extends Document {
  id: string;
  content: string;
  username: string;
  movieId: number;
  likes: CommentLike[];
}

const CommentSchema: Schema = new Schema({
  id: {
    type: String
  },
  content: {
    type: String,
    max: 500
  },
  postedBy: {
    type: ObjectId,
    ref: 'User'
  },
  movieId: {
    type: Number,
    required: true
  },
  likes: [{type: ObjectId, ref: "User"}]
}, {timestamps: true});

export const Comment: Model<IComment> = model('Comment', CommentSchema);
