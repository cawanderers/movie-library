import Mongoose, { Schema, model, Model, Document } from 'mongoose';

const {ObjectId} = Mongoose.Schema.Types;

export interface IRating extends Document {
  ID: string;
  rate: number;
  username: string;
  movieId: number;
}

const RatingSchema: Schema = new Schema({
  id: {
    type: String
  },
  rate: {
    type: Number
  },
  ratedBy: {
    type: ObjectId,
    ref: 'User'
  },
  movieId: {
    type: Number,
    required: true
  }
});

export const Rating: Model<IRating> = model('Rating', RatingSchema);
