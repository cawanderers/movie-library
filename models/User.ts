import Mongoose, { Schema, model, Model, Document } from 'mongoose';
const bcrypt = require('bcrypt');

export interface IUser extends Document {
  email: string;
  password: string;
  role: string;
  username: string;
  comparePassword: comparePasswordFunction;
}

type comparePasswordFunction = (password: string, cb: (err: any, isMatch: any) => void) => void;

const UserSchema: Schema = new Schema({
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  role: {
    type: String,
    required: true,
    enum: ['user', 'admin']
  },
  username: {
    type: String,
    required: true,
    max: 30,
    min: 6
  }
});

UserSchema.pre<IUser>('save', function (next){
  if(!this.isModified('password')) {
    return next();
  }
  bcrypt.hash(this.password,10,(err: any,passwordHash: any)=> {
    if(err) {
      return next(err);
    }
    this.password = passwordHash;
    next();
  });
});

const comparePassword: comparePasswordFunction = function (this: any, password, cb) {
  bcrypt.compare(password, this.password, (err: Mongoose.Error, isMatch: boolean) => {
    cb(err, isMatch);
  });
};

UserSchema.methods.comparePassword = comparePassword;

export const User: Model<IUser> = model('User', UserSchema);
