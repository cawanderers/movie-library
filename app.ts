import express from 'express';
import Mongoose from 'mongoose';

const userRouter = require('./routes/User');
const profileRouter = require('./routes/Profile');
const commentRouter = require('./routes/Comment');
const ratingRouter = require('./routes/Rating');
const collectedMovieRouter = require('./routes/CollectedMovie');
const favoriteMovieRouter = require('./routes/FavoriteMovie');

const app = express();
const cookieParser = require('cookie-parser');

app.use(express.urlencoded({extended: true}));
app.use(cookieParser());
app.use(express.json());
app.use('/api/user', userRouter);
app.use('/api/user/profile', profileRouter);
app.use('/api/movie/comment', commentRouter);
app.use('/api/movie/rating', ratingRouter);
app.use('/api/movie/collectedMovie', collectedMovieRouter);
app.use('/api/movie/favoriteMovie', favoriteMovieRouter);

const uri = "mongodb+srv://f117qaz:57829189@cluster0.g5eib.mongodb.net/?retryWrites=true&w=majority";
Mongoose.connect(uri, {useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true})
  .then(() => {
    console.log('Connected to Database');
  })
  .catch(error => console.error(error));

app.get('/', function (req, res) {
  res.send('Hello World');
});

app.listen(5000, () => {
  console.log('listening on 5000');
});
