import { IFavoriteMovie, FavoriteMovie } from "../models/FavoriteMovie";
import { Response, Request } from 'express';

const favoriteMovieRouter = require('express').Router();
const passport = require('passport');

declare global {
  namespace Express {
    interface Request {
      user?: any;
    }
  }
}

favoriteMovieRouter.get('/getMyFavoriteMovie', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  try {
    FavoriteMovie.find({markedBy: req.user._id})
      .populate("markedBy", "_id username")
      .then(myFavoriteMovie => {
        res.status(200).json({myFavoriteMovie});
      })
  } catch (err) {
    res.status(400).json({message: {msgBody: '/getMyFavoriteMovie Error has occurred', msgError: true}});
    console.log(err);
  }
});

favoriteMovieRouter.post('/markAsMyFavoriteMovie', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  const { movieId, title, poster, rating, overview, releaseDate } = req.body;
  const myFavoriteMovie: IFavoriteMovie = new FavoriteMovie({
    movieId, title, poster, rating, overview, releaseDate, markedBy: req.user
  });
  try {
    await myFavoriteMovie.save();
    FavoriteMovie.find({markedBy: req.user._id})
      .populate("markedBy", "_id username")
      .then(myFavoriteMovie => {
        res.status(200).json({myFavoriteMovie});
      })
  } catch (err) {
    res.status(400).json({message: {msgBody: '/markAsMyFavoriteMovie Error has occurred', msgError: true}});
    console.log(err);
  }
});

favoriteMovieRouter.patch('/updateMyFavoriteMovie/:id', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  const { movieId, title, poster, rating, overview, releaseDate } = req.body;
  const updatedFavoriteMovie = {id: req.params.id, movieId, title, poster, rating, overview, releaseDate, markedBy: req.user};
  try {
    await FavoriteMovie.findByIdAndUpdate(req.params.id, updatedFavoriteMovie, {new: true});
    FavoriteMovie.find({markedBy: req.user._id})
      .populate("markedBy", "_id username")
      .then(myFavoriteMovie => {
        res.status(200).json({myFavoriteMovie});
      })
  } catch (err) {
    res.status(400).json({message: {msgBody: '/updateMyFavoriteMovie Error has occurred', msgError: true}});
    console.log(err);
  }
})

favoriteMovieRouter.delete('/unmarkAsMyFavoriteMovie/:id', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  try {
    const movie = FavoriteMovie.findById(req.params.id);
    await movie?.remove();
    FavoriteMovie.find({markedBy: req.user._id})
      .populate("markedBy", "_id username")
      .then(myFavoriteMovie => {
        res.status(200).json({myFavoriteMovie, success: true});
      })
  } catch (err) {
    res.status(400).json({message: {msgBody: '/unmarkAsMyFavoriteMovie Error has occurred', msgError: true}});
    console.log(err);
  }
});

module.exports = favoriteMovieRouter;
