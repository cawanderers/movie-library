import { IProfile, Profile } from "../models/Profile";
import { Response, Request } from 'express';

const profileRouter = require('express').Router();
const passport = require('passport');

declare global {
  namespace Express {
    interface Request {
      user?: any;
    }
  }
}

profileRouter.get('/getMyProfile', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  try {
    Profile.find({editedBy: req.user._id})
      .populate('editedBy', '_id username')
      .then(myProfile => {
        res.status(200).json({myProfile});
      })
  } catch (err) {
    res.status(400).json({message: {msgBody: '/getMyProfile Error has occurred', msgError: true}});
    console.log(err);
  }
});

profileRouter.post('/createMyProfile', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  const { firstName, lastName, gender, birthday, about, favouriteMovieGenre } = req.body;
  const myProfile: IProfile = new Profile({
    firstName, lastName, gender, birthday, about, favouriteMovieGenre, editedBy: req.user
  });
  try {
    await myProfile.save();
    Profile.find({editedBy: req.user._id})
      .populate('editedBy', '_id username')
      .then(myProfile => {
        res.status(200).json({myProfile});
      })
  } catch (err) {
    res.status(400).json({message: {msgBody: '/createMyProfile Error has occurred', msgError: true}});
    console.log(err);
  }
});

profileRouter.patch('/updateMyProfile/:id', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  const { firstName, lastName, gender, birthday, about, favouriteMovieGenre } = req.body;
  const updatedProfile = {id: req.params.id, firstName, lastName, gender, birthday, about, favouriteMovieGenre, editedBy: req.user};
  try {
    await Profile.findByIdAndUpdate(req.params.id, updatedProfile, {new: true});
    Profile.find({editedBy: req.user._id})
      .populate("editedBy", "_id username")
      .then(myProfile => {
        res.status(200).json({myProfile});
      })
  } catch (err) {
    res.status(400).json({message: {msgBody: '/updateMyProfile Error has occurred', msgError: true}});
    console.log(err);
  }
});

module.exports = profileRouter;
