import { ICollectedMovie, CollectedMovie } from "../models/CollectedMovie";
import { Response, Request } from 'express';

const collectedMovieRouter = require('express').Router();
const passport = require('passport');

declare global {
  namespace Express {
    interface Request {
      user?: any;
    }
  }
}

collectedMovieRouter.get('/getMyWatchlist', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  try {
    CollectedMovie.find({collectedBy: req.user._id})
      .populate("collectedBy", "_id username")
      .then(myCollectedMovies => {
        res.status(200).json({myCollectedMovies});
      })
  } catch (err) {
    res.status(400).json({message: {msgBody: '/getMyWatchlist Error has occurred', msgError: true}});
    console.log(err);
  }
});

collectedMovieRouter.post('/addToWatchlist', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  const { movieId, title, poster, rating, overview, releaseDate } = req.body;
  const collectedMovie: ICollectedMovie = new CollectedMovie({
    movieId, title, poster, rating, overview, releaseDate, collectedBy: req.user
  });
  try {
    await collectedMovie.save();
    CollectedMovie.find({collectedBy: req.user._id, 'movieId': req.body.movieId})
      .populate("collectedBy", "_id username")
      .then(movie => {
        res.status(200).json({movie});
      })
  } catch (err) {
    res.status(400).json({message: {msgBody: '/addToWatchlist Error has occurred', msgError: true}});
    console.log(err);
  }
});

collectedMovieRouter.post('/getMovieFromWatchlist', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  try {
    CollectedMovie.find({collectedBy: req.user._id, 'movieId': req.body.movieId})
      .populate("collectedBy", "_id username")
      .then(movie => {
        res.status(200).json({movie});
      })
  } catch (err) {
    res.status(400).json({message: {msgBody: '/getMovieFromWatchlist Error has occurred', msgError: true}});
    console.log(err);
  }
});

collectedMovieRouter.delete('/removeFromWatchlist/:id', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
    try {
      const movie = await CollectedMovie.findById(req.params.id);
      await movie?.remove();
      CollectedMovie.find({collectedBy: req.user._id})
        .populate("collectedBy", "_id username")
        .then(myCollectedMovies => {
          res.status(200).json({myCollectedMovies, success: true});
        })
    } catch (err) {
      res.status(400).json({message: {msgBody: '/removeFromWatchlist Error has occurred', msgError: true}});
      console.log(err);
    }
  });

module.exports = collectedMovieRouter;
