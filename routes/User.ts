import { IUser, User } from '../models/User';
import { Response, Request } from 'express';

const userRouter = require('express').Router();
const passport = require('passport');
const passportConfig = require('../passport');
const JWT = require('jsonwebtoken');

const signToken = (userID: string) => {
  return JWT.sign({
    iss: 'SteveBai',
    sub: userID
  }, 'SteveBai', {expiresIn: '1h'});
};

declare global {
  namespace Express {
    interface Request {
      user?: any;
    }
  }
}

userRouter.post('/register', (req: Request, res: Response) => {
  const { email, password, role, username } = req.body;
  User.findOne({username}, (err: any, user: IUser) => {
    if (err) {
      res.status(500).json({message: {msgBody: 'Error has occurred', msgError: true}});
    }
    if (user) {
      res.status(400).json({message: {msgBody: 'Username is already taken', msgError: true}, fieldName: 'username'});
    } else {
      const newUser: IUser = new User({email, password, role, username});
      newUser.save()
        .then((response) => {
          if(!response) {
            return res.status(401).json({message: "Authentication failed",});
          }
          const {_id} = newUser;
          const token = signToken(_id);
          res.cookie('access_token', token, {httpOnly: true, sameSite: true});
          res.status(200).json({isAuthenticated: true, user: {username, email}, msgBody: "Account successfully created"});
        });
    }
  });
});

userRouter.post('/login', passport.authenticate('local', {session: false}), (req: Request, res: Response) => {
  const { _id, username, email } = req.user;
  if (req.isAuthenticated()) {
    const token = signToken(_id);
    res.cookie('access_token', token, {httpOnly: true, sameSite: true});
    res.status(200).json({isAuthenticated: true, user: {username, email}, msgBody: "Account successfully logged in"});
  }
});

userRouter.get('/logout', passport.authenticate('jwt', {session: false}), (req: Request, res: Response) => {
  res.clearCookie('access_token');
  res.json({user: {user: '', role: ''}, success: true});
});

userRouter.get('/admin', passport.authenticate('jwt', {session: false}), (req: Request, res: Response) => {
  if (req?.user?.role === 'admin') {
    res.status(200).json({message: {msgBody: 'You are an admin', msgError: false}});
  } else {
    res.status(403).json({message: {msgBody: "You're not an admin", msgError: true}});
  }
});

userRouter.get('/authenticated', passport.authenticate('jwt', {session: false}), (req: Request, res: Response) => {
  const {email, username} = req.user;
  res.status(200).json({isAuthenticated: true, user: {email, username}});
});

module.exports = userRouter;
