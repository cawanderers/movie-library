import { IRating, Rating } from "../models/Rating";
import { Response, Request } from 'express';

const ratingRouter = require('express').Router();
const passport = require('passport');

declare global {
  namespace Express {
    interface Request {
      user?: any;
    }
  }
}

ratingRouter.post('/createMovieRating', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  const { rate, movieId } = req.body;
  const rating: IRating = new Rating({
    rate, ratedBy: req.user, movieId
  })
  try {
    await rating.save();
    const ratings = await Rating.find({'movieId': req.body.movieId})
      .populate('postedBy', 'username')
      .sort('-createdAt');
    res.status(201).json({ratings, message: {msgBody: "Rating successfully created", msgError: false}});
  } catch (err) {
    res.status(201).json({message: {msgBody: "/createRating Error has occurred", msgError: true}});
    console.log(err);
  }
});

ratingRouter.post('/getMovieRatings', async (req: Request, res: Response) => {
  try {
    const ratings = await Rating.find({'movieId': req.body.movieId})
      .populate('ratedBy', 'username');
    res.status(200).json({ratings, msgError: false});
  } catch (err) {
    res.status(400).json({message: {msgBody: '/getMovieRatings Error has occurred', msgError: true}});
    console.log(err);
  }
});

ratingRouter.get('/getMyMoviesRatings', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  try {
    Rating.find({ratedBy: req.user._id})
      .populate("ratedBy", "_id username")
      .then(myRatings => {
        res.status(200).json({myRatings});
      })
  } catch (err) {
    res.status(400).json({message: {msgBody: '/getMyMovieRatings Error has occurred', msgError: true}});
    console.log(err);
  }
});

ratingRouter.post('/getMyMovieRating', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  try {
    Rating.find({ratedBy: req.user._id, 'movieId': req.body.movieId})
      .populate("ratedBy", "_id username")
      .then(myRating => {
        res.status(200).json({myRating});
      })
  } catch (err) {
    res.status(400).json({message: {msgBody: '/getMyMovieRatings Error has occurred', msgError: true}});
    console.log(err);
  }
});

ratingRouter.patch('/updateMyMovieRating/:id', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  const { rate, movieId } = req.body;
  const updatedRate = {rate, movieId, ratedBy: req.user};
  try {
    await Rating.findByIdAndUpdate(req.params.id, updatedRate, {new: true});
    res.status(200).json(updatedRate);
  } catch (err) {
    res.status(400).json({message: {msgBody: '/updateMyMovieRating Error has occurred', msgError: true}});
    console.log(err);
  }
});

module.exports = ratingRouter;
