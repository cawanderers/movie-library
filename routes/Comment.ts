import { IComment, Comment } from "../models/Comment";
import { Response, Request } from 'express';

const commentRouter = require('express').Router();
const passport = require('passport');

declare global {
  namespace Express {
    interface Request {
      user?: any;
    }
  }
}

commentRouter.post('/createMovieComment', passport.authenticate('jwt', {session: false}), async (req: Request, res: Response) => {
  const {content, movieId} = req.body;
  if (!content) {
    res.status(422).json({error: "Please fill the field"})
  }
  const comment: IComment = new Comment({
    content, postedBy: req.user, movieId
  })
  try {
    await comment.save();
    const comments = await Comment.find({'movieId': req.body.movieId})
      .populate('postedBy', 'username')
      .sort('-createdAt');
    res.status(201).json({comments, message: {msgBody: "Comment successfully created", msgError: false}});
  } catch (err) {
    res.status(201).json({message: {msgBody: "/createComment Error has occurred", msgError: false}});
    console.log(err);
  }
});

commentRouter.post('/getMovieComments', async (req: Request, res: Response) => {
  try {
    const comments = await Comment.find({'movieId': req.body.movieId})
      .populate('postedBy', 'username')
      .sort('-createdAt');
    res.status(200).json({comments, msgError: false});
  } catch (err) {
    res.status(400).json({message: {msgBody: '/getMovieComments Error has occurred', msgError: true}});
    console.log(err);
  }
});

module.exports = commentRouter;
